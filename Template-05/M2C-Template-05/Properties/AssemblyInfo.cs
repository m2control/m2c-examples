﻿using System.Reflection;
using System.Runtime.InteropServices;
using MM.M2Control;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("M2Control Template-05")]
[assembly: AssemblyProduct("M2Control Template Plugin")]
[assembly: AssemblyDescription("Contains an example template implementation")]

[assembly: AssemblyCompany(M2ControlInfo.M2ControlTeam)]
[assembly: AssemblyCopyright(M2ControlInfo.M2ControlCopyright)]
[assembly: AssemblyVersion(M2ControlInfo.Version)]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
