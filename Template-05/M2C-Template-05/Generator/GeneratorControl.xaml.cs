﻿using System.Windows;
using System.Windows.Controls;

namespace M2C.Example.Template.Generator
{
	/// <summary>
	/// Interaction logic for GeneratorControl.xaml
	/// </summary>
	internal partial class GeneratorControl : UserControl
	{
		public static readonly DependencyProperty ModelProperty = DependencyProperty.Register(
			"Model", typeof(GeneratorViewModel), typeof(GeneratorControl), new PropertyMetadata(default(GeneratorViewModel)));

		public GeneratorViewModel Model
		{
			get { return (GeneratorViewModel)GetValue(ModelProperty); }
			set { SetValue(ModelProperty, value); }
		}

		public GeneratorControl()
		{
			InitializeComponent();
		}
	}
}
