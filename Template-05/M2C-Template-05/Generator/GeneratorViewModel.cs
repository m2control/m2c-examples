﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using M2C.Example.Template.Layer;
using M2C.Example.Template.Preview;
using M2C.Example.Template.Snapshot;
using MM.M2Control.Interface;
using MM.M2Control.Wpf;

namespace M2C.Example.Template.Generator
{
	/// <summary>
	/// model is used separate the data and ui from the template data.
	/// this way it's easy to reuse it to support another device.
	/// </summary>
	class GeneratorViewModel : INotifyPropertyChanged
	{
		private TemplateItem mSelectedItem;
		private string mPlaylistName;
		private string mGroupName;
		private string mMessage;
		private bool mUseGroup;

		private readonly GeneratorData mTemplateData;
		private readonly ILogger mLogger;

		/// <summary>
		/// use dependency injection to pass the templateData and host logger to the view model
		/// </summary>
		/// <param name="templateData"></param>
		/// <param name="logger"></param>
		public GeneratorViewModel(GeneratorData templateData, ILogger logger)
		{
			mTemplateData = templateData;
			mLogger = logger;

			Templates = new List<TemplateItem>();

			InitTemplateList();

			PlaylistName = "";
			GroupName = "Group";

			// we can bind commands to the control to keep functionality inside
			// the model. that way the control can easily be replaced or changed
			// without changed to controls code behind file
			CmdGenerate = new DelegateCommand(CmdGenerateExecuteAction, CmdGenerateCanExecute);
		}

		#region Properties

		public string PlaylistName
		{
			get { return mPlaylistName; }
			set { mPlaylistName = value; OnPropertyChanged(); }
		}

		public string GroupName
		{
			get { return mGroupName; }
			set { mGroupName = value; OnPropertyChanged(); }
		}

		public bool UseGroup
		{
			get { return mUseGroup; }
			set { mUseGroup = value; OnPropertyChanged(); }
		}

		public List<TemplateItem> Templates { get; set; }

		public TemplateItem SelectedItem
		{
			get { return mSelectedItem; }
			set
			{
				if (value == mSelectedItem) return;
				mSelectedItem = value;
				OnPropertyChanged();
			}
		}

		public string Message
		{
			get { return mMessage; }
			set { mMessage = value; OnPropertyChanged(); }
		}

		#endregion

		#region Methods
		/// <summary>
		/// Init the list of templates of our plugin dll
		/// </summary>
		private void InitTemplateList()
		{
			Templates.Clear();

			{
				var ti = new TemplateItem
				{
					TemplateGuid = LayerPlugin.cGUID,
					Title = LayerPlugin.cTitle
				};
				Templates.Add(ti);
			}

			{
				var ti = new TemplateItem
				{
					TemplateGuid = PreviewPlugin.cGUID,
					Title = PreviewPlugin.cTitle
				};
				Templates.Add(ti);
			}

			{
				var ti = new TemplateItem
				{
					TemplateGuid = SnapshotPlugin.cGUID,
					Title = SnapshotPlugin.cTitle
				};
				Templates.Add(ti);
			}
		}
#endregion

		#region Commands

		public ICommand CmdGenerate { get; }

		/// <summary>
		/// this is called on ui refresh to define the state of the command
		/// returning false will disable the button
		/// </summary>
		/// <param name="arg"></param>
		/// <returns></returns>
		private bool CmdGenerateCanExecute(object arg)
		{
			return !string.IsNullOrEmpty(PlaylistName)
				&& (!UseGroup || !string.IsNullOrEmpty(PlaylistName));
		}

		/// <summary>
		/// the command gets executed. here we we communicate with device
		/// </summary>
		/// <param name="obj"></param>
		private void CmdGenerateExecuteAction(object obj)
		{
			try
			{
				Message = "";

				var tpls = Templates.FindAll(k => k.IsChecked);
				if (tpls.Count <= 0)
				{
					Message = "Please check templates first";
					return;
				}

				mTemplateData.GeneratePlaylist(tpls);
			}
			catch (Exception e)
			{
				mLogger.LogCatch(nameof(CmdGenerateExecuteAction), e);
			}
		}
		#endregion

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}

	internal class TemplateItem : INotifyPropertyChanged
	{
		private bool mIsChecked;
		private string mTitle;

		public bool IsChecked
		{
			get { return mIsChecked; }
			set { mIsChecked = value; OnPropertyChanged(); }
		}

		public string Title
		{
			get { return mTitle; }
			set { mTitle = value; OnPropertyChanged(); }
		}

		public string TemplateGuid { get; set; }

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
