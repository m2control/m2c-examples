﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template.Generator
{
	/// <summary>
	/// these exports are used by M2Control to identify the template and its capabilities.
	/// here you define the device that the template can control and to which PluginDll it
	/// belongs. ensure that the TemplateGuid is unique in the system (VS->Tools->Create GUID).
	/// </summary>
	[Export(typeof(ITemplatePlugin))]
	[ExportMetadata(kTemplatePluginDllGuid, PluginDll.cGUID)]
	[ExportMetadata(kTemplateGuid, "61449F53-CCED-455C-8A76-FFCACE095DB3")]
	[ExportMetadata(kTemplateName, "Generator")]
	[ExportMetadata(kTemplateDescription, "A simple template that generates playlist")]
	[ExportMetadata(kTemplateInstanceRule, TemplateInstanceRuleType.Global)]
	[ExportMetadata(kTemplateDeviceHandler, KnownDevices.kVizEngineDeviceHandlerGuid)]
	[ExportMetadata(kTemplateDefaultAutomationID, "Generator")]
	[ExportMetadata(kTemplateMenuGroup, "M2C Examples")]
	[ExportMetadata(kTemplateIcon, TemplateIconType.ClipboardFile)]
	public class GeneratorPlugin : TemplatePluginBase
	{
		// be careful, constructor is called immediately on dll load, before everything else!
		public override ITemplateData InitializeTemplateData(ITemplateDataInitBlock initBlock,
			List<ITemplateDataValue> data)
		{
			return new GeneratorData(initBlock, this);
		}
	}
}
