﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using MM.M2Control.Interface;
using MM.M2Control.Vizrt;

namespace M2C.Example.Template.Generator
{
	/// <summary>
	/// template data inherits from a template base class to allow usage of the related device
	/// capabilities. override methods of the base class to have full control over sending data
	/// to a device. the base class gets accessible by referencing the M2Control[Vizrt|...] dll.
	/// </summary>
	class GeneratorData : VizTemplateDataBase
	{
		private readonly GeneratorViewModel mModel;

		/// <summary>
		/// this when playlist items get initialized for example opening the playlist
		/// where it is used. the initBlock allows the template to access the interfaces
		/// provided by the client.
		/// </summary>
		/// <param name="initBlock"></param>
		/// <param name="template"></param>
		public GeneratorData(ITemplateDataInitBlock initBlock, ITemplatePlugin template)
			: base(initBlock, template)
		{
			mModel = new GeneratorViewModel(this, initBlock.PluginHost.LoggerHandler);
		}

		public override UserControl BuildGui()
		{
			return new GeneratorControl {Model = mModel};
		}

		#region Playlist Generation

		/// <summary>
		/// creates a playlist with given items. optional items get created inside a group
		/// </summary>
		/// <param name="tpls"></param>
		/// <returns></returns>
		public void GeneratePlaylist(List<TemplateItem> tpls)
		{
			var pi = PluginHost.Factory.CreatePlaylist();
			pi.Name = mModel.PlaylistName;

			IGroupPlaylistItem grp = null;

			// create group if flag is set
			if (mModel.UseGroup)
			{
				grp = PluginHost.Factory.CreateGroupPlaylistItem();
				grp.Description = mModel.GroupName;
			}

			foreach (var tpl in tpls)
			{
				var tplGuid = new Guid(tpl.TemplateGuid);

				var tplVariant = PluginHost.HubDbHandler.GetFirstTemplateVariant(tplGuid);
				if (tplVariant == null)
				{
					mModel.Message += string.Format("Missing variant of template={0}", tpl.Title);
					continue;
				}

				var item = PluginHost.Factory.CreateTemplatePlaylistItem(tplGuid, tplVariant.Name, Guid.Empty);
				item.Description = tpl.Title;

				// add items to group if exists
				if (grp != null)
				{
					grp.AddItem(item);
					continue;
				}

				// no grouping - add directly to playlist
				pi.AddItem(item);
			}

			// optional add group to playlist
			if (grp != null)
			{
				pi.AddItem(grp);
			}

			PluginHost.HubDbHandler.ImportPlaylist(pi, "", true);

			mModel.Message += string.Format("Imported playlist={0} to playlists root", pi.Name);
		}

		#endregion
	}
}