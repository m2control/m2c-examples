﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using MM.M2Control.Interface;
using MM.M2Control.Vizrt;
using MM.M2Control.Wpf;

namespace M2C.Example.Template.Layer
{
	/// <summary>
	/// model is used separate the data and ui from the template data.
	/// this way it's easy to reuse it to support another device.
	/// </summary>
	class LayerViewModel : INotifyPropertyChanged
	{
		private const string cKeyText = "text";
		private const string cKeyLayer = "layer";

		private readonly LayerData mTemplateData;
		private readonly ILogger mLogger;

		private string mTextField;
		private string mLayer;


		/// <summary>
		/// use dependency injection to pass the templateData and host logger to the view model
		/// </summary>
		/// <param name="templateData"></param>
		/// <param name="logger"></param>
		public LayerViewModel(LayerData templateData, ILogger logger)
		{
			mTemplateData = templateData;
			mLogger = logger;
			// we can bind commands to the control to keep functionality inside
			// the model. that way the control can easily be replaced or changed
			// without changed to controls code behind file
			CmdSend = new DelegateCommand(CmdSendExecuteAction, CmdSendCanExecute);

			mTemplateData.ClearModified();
		}


		#region Properties

		/// <summary>
		/// just a text property the user can change in the user interface
		/// </summary>
		public string TextField
		{
			get { return mTextField; }
			set { mTextField = value; OnPropertyChanged(); }
		}

		/// <summary>
		/// current layer mode  the user can select it in the user interface
		/// </summary>
		public string Layer
		{
			get { return mLayer; }
			set { mLayer = value; OnPropertyChanged(); }
		}

		/// this is for combo only
		public List<string> Layers { get; set; }

		#endregion

		#region Methods

		/// <summary>
		/// return a list of values you want to store inside the playlist
		/// </summary>
		/// <param name="factory"></param>
		/// <returns></returns>
		public List<ITemplateDataValue> GetDataValues(IClassFactory factory)
		{
			return new List<ITemplateDataValue>
			{
				factory.CreateTemplateDataValue(cKeyText, TextField),
				factory.CreateTemplateDataValue(cKeyLayer, Layer),
			};
		}

		/// <summary>
		/// set the property values using the data stored in the list
		/// </summary>
		/// <param name="data"></param>
		/// <param name="values"></param>		
		public void SetDataValues(TemplateDataBase data, List<ITemplateDataValue> values)
		{
			TextField = data.GetDataValue(values, cKeyText, "");

			var elr = data.GetDataValue(values, cKeyLayer, "");
			Layer = Layers.Contains(elr) ? elr : VizMacros.kVizMiddleLayer;
		}

		#endregion

		#region Commands

		public ICommand CmdSend { get; }

		/// <summary>
		/// this is called on ui refresh to define the state of the command
		/// returning false will disable the button
		/// </summary>
		/// <param name="arg"></param>
		/// <returns></returns>
		private bool CmdSendCanExecute(object arg)
		{
			return !string.IsNullOrEmpty(TextField) && !string.IsNullOrEmpty(Layer);
		}

		/// <summary>
		/// the command gets executed. here we send to device
		/// </summary>
		/// <param name="obj"></param>
		private void CmdSendExecuteAction(object obj)
		{
			try
			{
				// use template data to send commands to selected layer 
				// this will throw if there's no matching device (here viz) is available
				mTemplateData.SendButtonMessageEx(Layer, TextField);
			}
			catch (Exception e)
			{
				mLogger.LogCatch(nameof(CmdSendExecuteAction), e);
			}
		}

		public void OnTake()
		{
			if (!string.IsNullOrEmpty(TextField) && !string.IsNullOrEmpty(Layer))
				mTemplateData.SendButtonMessageEx(Layer, TextField);
		}

		#endregion

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

			mTemplateData.Touch();
		}
	}
}
