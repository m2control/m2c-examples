﻿using System.Windows;
using System.Windows.Controls;

namespace M2C.Example.Template.Layer
{
	/// <summary>
	/// Interaction logic for TemplateControl.xaml
	/// </summary>
	internal partial class LayerControl : UserControl
	{
		public static readonly DependencyProperty ModelProperty = DependencyProperty.Register(
			"Model", typeof(LayerViewModel), typeof(LayerControl), new PropertyMetadata(default(LayerViewModel)));

		public LayerViewModel Model
		{
			get { return (LayerViewModel) GetValue(ModelProperty); }
			set { SetValue(ModelProperty, value); }
		}

		public LayerControl()
		{
			InitializeComponent();
		}
	}
}
