﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template.Layer
{
	/// <summary>
	/// these exports are used by M2Control to identify the template and its capabilities.
	/// here you define the device that the template can control and to which PluginDll it
	/// belongs. ensure that the TemplateGuid is unique in the system (VS->Tools->Create GUID).
	/// </summary>
	[Export(typeof(ITemplatePlugin))]
	[ExportMetadata(kTemplatePluginDllGuid, PluginDll.cGUID)]
	[ExportMetadata(kTemplateGuid, cGUID)]
	[ExportMetadata(kTemplateName, "Layer Viz Template")]
	[ExportMetadata(kTemplateDescription, "A simple template to test viz layer command redirection")]
	[ExportMetadata(kTemplateInstanceRule, TemplateInstanceRuleType.MultipleInPlaylist)]
	[ExportMetadata(kTemplateDeviceHandler, KnownDevices.kVizEngineDeviceHandlerGuid)]
	[ExportMetadata(kTemplateDefaultScene, "")]
	[ExportMetadata(kTemplateDefaultAutomationID, "LAYERTEST")]
	[ExportMetadata(kTemplateIcon, TemplateIconType.Gift)]
	public class LayerPlugin : TemplatePluginBase
	{
		public const string cGUID = "8CC80D11-ECF6-443D-B876-E4B40F1F6BCB";
		public const string cTitle = "Layer Viz Template";

		// be careful, constructor is called immediately on dll load, before everything else!
		public override ITemplateData InitializeTemplateData(ITemplateDataInitBlock initBlock, List<ITemplateDataValue> data)
		{
			return new LayerData(initBlock, this, data);
		}
	}
}
