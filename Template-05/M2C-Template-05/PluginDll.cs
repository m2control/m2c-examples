﻿using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template
{
	[Export(typeof(ITemplatePluginDll))]
	[ExportMetadata(kPluginDllGuid, PluginDll.cGUID)]
	[ExportMetadata(kPluginDllName, @"M2C Example 05")]
	[ExportMetadata(kPluginDllDesc, @"Example implementation of viz templates")]
	[ExportMetadata(kPluginDllGroup, "M2C Examples")]
	public class PluginDll : TemplatePluginDllBase
	{
		public const string cGUID = @"f2ff08db-90b5-4be0-9004-be5d8c75c785";
	}
}