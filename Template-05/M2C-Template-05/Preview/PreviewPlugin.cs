﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template.Preview
{
	/// <summary>
	/// these exports are used by M2Control to identify the template and its capabilities.
	/// here you define the device that the template can control and to which PluginDll it
	/// belongs. ensure that the TemplateGuid is unique in the system (VS->Tools->Create GUID).
	/// </summary>
	[Export(typeof(ITemplatePlugin))]
	[ExportMetadata(kTemplatePluginDllGuid, PluginDll.cGUID)]
	[ExportMetadata(kTemplateGuid, cGUID)]	
	[ExportMetadata(kTemplateName, cTitle)]
	[ExportMetadata(kTemplateDescription, "A simple template to make previews")]
	[ExportMetadata(kTemplateInstanceRule, TemplateInstanceRuleType.MultipleInPlaylist)]
	[ExportMetadata(kTemplateDeviceHandler, KnownDevices.kVizEngineDeviceHandlerGuid)]
	[ExportMetadata(kTemplateDefaultScene, "Examples/Example05_Preview")]
	[ExportMetadata(kTemplateDefaultAutomationID, "Preview")]
	[ExportMetadata(kTemplateIcon, TemplateIconType.Film)]
	public class PreviewPlugin : TemplatePluginBase
	{
		public const string cGUID = "A11188F0-06CB-4964-A8E0-D0EC8EF6AA88";
		public const string cTitle = "Previewer";

		// be careful, constructor is called immediately on dll load, before everything else!
		public override ITemplateData InitializeTemplateData(ITemplateDataInitBlock initBlock, List<ITemplateDataValue> data)
		{
			var settings = new TemplateSettings();
			settings.Load(PluginHost);

			return new PreviewData(initBlock, this, data, settings);
		}
	}
}
