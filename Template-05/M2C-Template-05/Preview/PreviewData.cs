﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Controls;
using MM.M2Control.Interface;
using MM.M2Control.Vizrt;
using MM.M2Control.Wpf.Extensions;

namespace M2C.Example.Template.Preview
{
	/// <summary>
	/// template data inherits from a template base class to allow usage of the related device
	/// capabilities. override methods of the base class to have full control over sending data
	/// to a device. the base class gets accessible by referencing the M2Control[Vizrt|...] dll.
	/// </summary>
	class PreviewData : VizTemplateDataBase
	{
		private readonly PreviewViewModel mModel;

		/// <summary>
		/// this when playlist items get initialized for example opening the playlist
		/// where it is used. the initBlock allows the template to access the interfaces
		/// provided by the client. the data contains template data saved for
		/// the correspondent playlist item. the settings contain template settings from
		/// M2Control->Configuration->Variables
		/// </summary>
		/// <param name="initBlock"></param>
		/// <param name="template"></param>
		/// <param name="data"></param>
		/// <param name="settings"></param>
		public PreviewData(ITemplateDataInitBlock initBlock, ITemplatePlugin template,
			List<ITemplateDataValue> data, TemplateSettings settings)
			: base(initBlock, template)
		{
			mModel = new PreviewViewModel(this, initBlock.PluginHost.LoggerHandler);
			mModel.Filename = settings.FileName;

			SetDataValuesInternal(data);
		}

		/// <summary>
		/// return the control for this template - in this case its an wpf control bound
		/// to our view model
		/// </summary>
		/// <returns></returns>
		public override UserControl BuildGui()
		{
			return new PreviewControl { Model = mModel };
		}
		
		/// <summary>
		/// this is called on save an opened playlist template
		/// </summary>
		/// <returns></returns>
		public override List<ITemplateDataValue> GetDataValues()
		{
			return mModel.GetDataValues(PluginHost.Factory);			
		}

		/// <summary>
		/// template data was changed so m2c will pass changes here
		/// </summary>
		/// <param name="data"></param>
		public override void SetDataValues(List<ITemplateDataValue> data)
		{
			SetDataValuesInternal(data);
		}

		private void SetDataValuesInternal(List<ITemplateDataValue> data)
		{
			if (data == null)
				return;

			mModel.SetDataValues(this, data);
		}

		/// <summary>
		/// the scene has 3 directors; we prepare 3 farmes each with 
		/// text for textfield and positions for 3 directors
		/// </summary>
		/// <param name="scene"></param>
		/// <param name="destfolder"></param>
		public void CreatePreview(string scene, string destfolder)
		{
			var pframes = new List<IDeviceTransportPreviewFrame>();
			const int cFrames = 4;

			// "Fnn" - absolute position nn; 
			// "$cccc" - stop position with name=cccc
			pframes.Add(CreateFrame($"{destfolder}\\F1", "F13", "F0", "F0"));
			pframes.Add(CreateFrame($"{destfolder}\\F2", "$pilot1", "$pause", "F0"));
			pframes.Add(CreateFrame($"{destfolder}\\F3", "$pilot1", "F45", "$Pause"));
			pframes.Add(CreateFrame($"{destfolder}\\F4", "F96", "F45", "F50"));

			// Call device
			var prequest = PluginHost.Factory.CreateDeviceTransportPreviewRequestData(pframes);
			var frameImages = GetPreviewBytes(scene, 1920 / 4, 1080 / 4, false, ImageFormatType.kImageFormatPng, prequest);

			if (frameImages.Count == 0)
				throw new ControlUnexpectedException("GetPreviewBytes returned empty list");
			if (frameImages.Count != cFrames)
				throw new ControlUnexpectedException("GetPreviewBytes returned less frames");

			Directory.CreateDirectory(destfolder);

			// write frameimages to destfolder
			for (var i = 0; i < frameImages.Count; i++)
			{
				var findex = i + 1;
				var frameImage = frameImages[i];

				if (frameImage.Data == null)
					throw new ControlUnexpectedException("GetPreviewBytes frameImage.Data null, frame=" + findex);

				PluginHost.LoggerHandler.LogAddDebug(string.Format("GetPreviewBytes done. length={0} {1}x{2} frame={3}", frameImage.Data.Length, frameImage.Width, frameImage.Height, findex));

				//var dest = Path.Combine(destfolder, string.Format("{0}-F{1}.png", fn, findex));
				var dest = Path.Combine(destfolder, $"F{findex}.png");
				File.WriteAllBytes(dest, frameImage.Data);
			}
		}

		/// <summary>
		/// create frame with 3 positions (for 3 directors)
		/// and with one command which send text to textfield
		/// </summary>
		/// <param name="text"></param>
		/// <param name="dirpos1"></param>
		/// <param name="dirpos2"></param>
		/// <param name="dirpos3"></param>
		/// <returns></returns>
		private IDeviceTransportPreviewFrame CreateFrame(string text, string dirpos1, string dirpos2, string dirpos3)
		{
			var pframe = PluginHost.Factory.CreateDeviceTransportPreviewFrame();

			//pframe.ControlCommandStrings.Add(string.Format("RENDERER*TREE*$text1*GEOM*TEXT SET look:{0}-F{1}", fn, framio));
			pframe.ControlCommandStrings.Add($"RENDERER*TREE*$text1*GEOM*TEXT SET look:{text}");

			{
				{
					var cpo = PluginHost.Factory.CreateDeviceTransportPreviewCuePoint();
					cpo.AnimationDirector = "Default";
					cpo.TimePosition = dirpos1;
					pframe.ICuePoints.Add(cpo);
				}

				{
					var cpo = PluginHost.Factory.CreateDeviceTransportPreviewCuePoint();
					cpo.AnimationDirector = "sponsor";
					cpo.TimePosition = dirpos2;
					pframe.ICuePoints.Add(cpo);
				}

				{
					var cpo = PluginHost.Factory.CreateDeviceTransportPreviewCuePoint();
					cpo.AnimationDirector = "Header";
					cpo.TimePosition = dirpos3;
					pframe.ICuePoints.Add(cpo);
				}
			}
			return pframe;
		}
	}
}
