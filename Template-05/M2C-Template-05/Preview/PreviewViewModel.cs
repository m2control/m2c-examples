﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using MM.M2Control.Interface;
using MM.M2Control.Vizrt;
using MM.M2Control.Wpf;

namespace M2C.Example.Template.Preview
{
	/// <summary>
	/// model is used separate the data and ui from the template data.
	/// this way it's easy to reuse it to support another device.
	/// </summary>
	class PreviewViewModel : INotifyPropertyChanged
	{
		private string mFilename;
		private string mScene;

		private const string cSceneKey = "sc";

		private readonly PreviewData mTemplateData;
		private readonly ILogger mLogger;

		/// <summary>
		/// use dependency injection to pass the templateData and host logger to the view model
		/// </summary>
		/// <param name="templateData"></param>
		/// <param name="logger"></param>
		public PreviewViewModel(PreviewData templateData, ILogger logger)
		{
			mTemplateData = templateData;
			mLogger = logger;

			mTemplateData.ClearModified();

			// we can bind commands to the control to keep functionality inside
			// the model. that way the control can easily be replaced or changed
			// without changed to controls code behind file
			CmdPreview = new DelegateCommand(CmdPreviewExecuteAction, CmdPreviewCanExecute);
		}


		#region Properties

		/// <summary>
		/// filename property the user can change in the user interface
		/// </summary>
		public string Filename
		{
			get { return mFilename; }
			set { mFilename = value; OnPropertyChanged(); }
		}
		/// <summary>
		/// scene property the user can change in the user interface
		/// </summary>
		public string Scene
		{
			get { return mScene; }
			set { mScene = value; OnPropertyChanged(); }
		}

		#endregion

		#region Methods

		/// <summary>
		/// return a list of values you want to store inside the playlist
		/// </summary>
		/// <param name="factory"></param>
		/// <returns></returns>
		public List<ITemplateDataValue> GetDataValues(IClassFactory factory)
		{
			return new List<ITemplateDataValue>
			{
				factory.CreateTemplateDataValue(cSceneKey, Scene),
			};
		}

		/// <summary>
		/// set the property values using the data stored in the list
		/// </summary>
		/// <param name="data"></param>
		/// <param name="values"></param>		
		public void SetDataValues(TemplateDataBase data, List<ITemplateDataValue> values)
		{
			Scene = data.GetDataValue(values, cSceneKey, "/Examples/Example05_Preview");
		}

		#endregion

		#region Commands

		public ICommand CmdPreview { get; }

		/// <summary>
		/// this is called on ui refresh to define the state of the command
		/// returning false will disable the button
		/// </summary>
		/// <param name="arg"></param>
		/// <returns></returns>
		private bool CmdPreviewCanExecute(object arg)
		{
			return !string.IsNullOrEmpty(Filename) && !string.IsNullOrEmpty(Scene);
		}

		/// <summary>
		/// the command gets executed. here we we communicate with device
		/// </summary>
		/// <param name="obj"></param>
		private void CmdPreviewExecuteAction(object obj)
		{
			try
			{
				var fn = Path.GetFileNameWithoutExtension(Filename);
				if (string.IsNullOrEmpty(fn))
				{
					MessageBox.Show("Meeh. filename empty");
					return;
				}

				var path = Path.GetDirectoryName(Filename);
				if (string.IsNullOrEmpty(path))
				{
					MessageBox.Show("Meeh. file path empty");
					return;
				}

				var destfolder = Path.Combine(path, $"{fn}-{DateTime.Now:yyyyMMdd-HHmmss}");

				mTemplateData.CreatePreview(Scene, destfolder);
			}
			catch (Exception e)
			{
				mLogger.LogCatch(nameof(CmdPreviewExecuteAction), e);
			}
		}
		#endregion

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

			mTemplateData.Touch();
		}
	}
}
