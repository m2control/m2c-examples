﻿using System.Windows;
using System.Windows.Controls;

namespace M2C.Example.Template.Preview
{
	/// <summary>
	/// Interaction logic for TemplateControl.xaml
	/// </summary>
	internal partial class PreviewControl : UserControl
	{
		public static readonly DependencyProperty ModelProperty = DependencyProperty.Register(
			"Model", typeof(PreviewViewModel), typeof(PreviewControl), new PropertyMetadata(default(PreviewViewModel)));

		public PreviewViewModel Model
		{
			get { return (PreviewViewModel) GetValue(ModelProperty); }
			set { SetValue(ModelProperty, value); }
		}

		public PreviewControl()
		{
			InitializeComponent();
		}
	}
}
