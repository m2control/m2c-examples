﻿using System;
using System.Collections.Generic;
using System.Linq;
using MM.M2Control.Interface;

namespace M2C.Example.Template.Preview
{
	/// <summary>
	/// this class is used to handle needed template settings.
	/// they can be edited under M2Control->Configuration->Variables	
	/// </summary>
	class TemplateSettings
	{
		public const string cHashConfigKey = "Template-05";

		private const string cFileNameKey = "preview.default_file";

		private static readonly List<Tuple<string, string, string>> sAllSettings =
			new List<Tuple<string, string, string>>
			{
				new Tuple<string, string, string>(cFileNameKey, "c:\\temp\\preview",""),
				// add more parameters here if needed...
			};

		/// <summary>
		/// load the configuration - if it's not created yet the default values of the tuple
		/// get used.
		/// </summary>
		/// <param name="host"></param>
		public void Load(ITemplatePluginHost host)
		{
			var conf = host.Factory.CreateHashConfig();

			var skeys = sAllSettings.Select(k => k.Item1).ToList();
			var svalues = sAllSettings.Select(k => k.Item2).ToList();

			conf.InitConfigHash(host.HashHandler, cHashConfigKey, skeys, svalues, host.GetCurrentUser());

			// read the value of our database key - it would throw if it's empty!
			FileName = conf.GetValueS_NonEmpty(cFileNameKey);
		}

		#region Properties
		public string FileName { get; private set; }

		#endregion
	}
}
