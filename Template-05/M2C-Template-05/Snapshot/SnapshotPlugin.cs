﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template.Snapshot
{
	/// <summary>
	/// these exports are used by M2Control to identify the template and its capabilities.
	/// here you define the device that the template can control and to which PluginDll it
	/// belongs. ensure that the TemplateGuid is unique in the system (VS->Tools->Create GUID).
	/// </summary>
	[Export(typeof(ITemplatePlugin))]
	[ExportMetadata(kTemplatePluginDllGuid, PluginDll.cGUID)]
	[ExportMetadata(kTemplateGuid, cGUID)]
	[ExportMetadata(kTemplateName, cTitle)]
	[ExportMetadata(kTemplateDescription, "A simple template to make Snapshot")]
	[ExportMetadata(kTemplateInstanceRule, TemplateInstanceRuleType.MultipleInPlaylist)]
	[ExportMetadata(kTemplateDeviceHandler, KnownDevices.kVizEngineDeviceHandlerGuid)]
	[ExportMetadata(kTemplateDefaultScene, "")]
	[ExportMetadata(kTemplateDefaultAutomationID, "Snapshot")]
	[ExportMetadata(kTemplateIcon, TemplateIconType.Camera)]
	public class SnapshotPlugin : TemplatePluginBase
	{
		public const string cGUID = "61A831B1-5A06-49A7-843A-AA7618249A67";
		public const string cTitle = "Snapshoter";

		// be careful, constructor is called immediately on dll load, before everything else!
		public override ITemplateData InitializeTemplateData(ITemplateDataInitBlock initBlock, List<ITemplateDataValue> data)
		{
			return new SnapshotData(initBlock, this, data);
		}
	}
}
