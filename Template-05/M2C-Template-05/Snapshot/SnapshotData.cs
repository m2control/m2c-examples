﻿using System.Collections.Generic;
using System.Windows.Controls;
using MM.M2Control.Interface;
using MM.M2Control.Vizrt;

namespace M2C.Example.Template.Snapshot
{
	/// <summary>
	/// template data inherits from a template base class to allow usage of the related device
	/// capabilities. override methods of the base class to have full control over sending data
	/// to a device. the base class gets accessible by referencing the M2Control[Vizrt|...] dll.
	/// </summary>
	class SnapshotData : VizTemplateDataBase
	{
		private readonly SnapshotViewModel mModel;

		/// <summary>
		/// this when playlist items get initialized for example opening the playlist
		/// where it is used. the initBlock allows the template to access the interfaces
		/// provided by the client.
		/// </summary>
		/// <param name="initBlock"></param>
		/// <param name="template"></param>
		public SnapshotData(ITemplateDataInitBlock initBlock, ITemplatePlugin template,
			List<ITemplateDataValue> data)
			: base(initBlock, template)
		{
			mModel = new SnapshotViewModel(this, initBlock.PluginHost.LoggerHandler);
			SetDataValuesInternal(data);
		}

		/// <summary>
		/// return the control for this template - in this case its an wpf control bound
		/// to our view model
		/// </summary>
		/// <returns></returns>
		public override UserControl BuildGui()
		{
			return new SnapshotControl { Model = mModel };
		}
		
		/// <summary>
		/// this is called on save an opened playlist template
		/// </summary>
		/// <returns></returns>
		public override List<ITemplateDataValue> GetDataValues()
		{
			return mModel.GetDataValues(PluginHost.Factory);			
		}

		/// <summary>
		/// template data was changed so m2c will pass changes here
		/// </summary>
		/// <param name="data"></param>
		public override void SetDataValues(List<ITemplateDataValue> data)
		{
			SetDataValuesInternal(data);
		}

		private void SetDataValuesInternal(List<ITemplateDataValue> data)
		{
			if (data == null)
				return;

			mModel.SetDataValues(this, data);
		}
	}
}
