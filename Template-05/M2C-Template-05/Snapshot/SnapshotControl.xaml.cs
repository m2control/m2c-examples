﻿using System.Windows;
using System.Windows.Controls;

namespace M2C.Example.Template.Snapshot
{
	/// <summary>
	/// Interaction logic for TemplateControl.xaml
	/// </summary>
	internal partial class SnapshotControl : UserControl
	{
		public static readonly DependencyProperty ModelProperty = DependencyProperty.Register(
			"Model", typeof(SnapshotViewModel), typeof(SnapshotControl), new PropertyMetadata(default(SnapshotViewModel)));

		public SnapshotViewModel Model
		{
			get { return (SnapshotViewModel) GetValue(ModelProperty); }
			set { SetValue(ModelProperty, value); }
		}

		public SnapshotControl()
		{
			InitializeComponent();
		}
	}
}
