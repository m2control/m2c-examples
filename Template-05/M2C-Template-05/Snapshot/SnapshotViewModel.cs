﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using MM.M2Control.Interface;
using MM.M2Control.Wpf;

namespace M2C.Example.Template.Snapshot
{
	/// <summary>
	/// model is used separate the data and ui from the template data.
	/// this way it's easy to reuse it to support another device.
	/// </summary>
	class SnapshotViewModel : INotifyPropertyChanged
	{
		private string mFilename;

		private const string cFilenameKey = "fn";

		private readonly SnapshotData mTemplateData;
		private readonly ILogger mLogger;

		/// <summary>
		/// use dependency injection to pass the templateData and host logger to the view model
		/// </summary>
		/// <param name="templateData"></param>
		/// <param name="logger"></param>
		public SnapshotViewModel(SnapshotData templateData, ILogger logger)
		{
			mTemplateData = templateData;
			mLogger = logger;
			// we can bind commands to the control to keep functionality inside
			// the model. that way the control can easily be replaced or changed
			// without changed to controls code behind file
			CmdSnapshot = new DelegateCommand(CmdSnapshotExecuteAction, CmdSnapshotCanExecute);

			mTemplateData.ClearModified();
		}


		#region Properties

		/// <summary>
		/// filename property the user can change in the user interface
		/// </summary>
		public string Filename
		{
			get { return mFilename; }
			set { mFilename = value; OnPropertyChanged(); }
		}

		#endregion

		#region Methods

		/// <summary>
		/// return a list of values you want to store inside the playlist
		/// </summary>
		/// <param name="factory"></param>
		/// <returns></returns>
		public List<ITemplateDataValue> GetDataValues(IClassFactory factory)
		{
			return new List<ITemplateDataValue>
			{
				factory.CreateTemplateDataValue(cFilenameKey, Filename),
			};
		}

		/// <summary>
		/// set the property values using the data stored in the list
		/// </summary>
		/// <param name="data"></param>
		/// <param name="values"></param>		
		public void SetDataValues(TemplateDataBase data, List<ITemplateDataValue> values)
		{
			Filename = data.GetDataValue(values, cFilenameKey, "c:\\temp\\snapshot");
		}

		#endregion

		#region Commands

		public ICommand CmdSnapshot { get; }

		/// <summary>
		/// this is called on ui refresh to define the state of the command
		/// returning false will disable the button
		/// </summary>
		/// <param name="arg"></param>
		/// <returns></returns>
		private bool CmdSnapshotCanExecute(object arg)
		{
			return !string.IsNullOrEmpty(Filename);
		}

		/// <summary>
		/// the command gets executed. here we send to device
		/// </summary>
		/// <param name="obj"></param>
		private void CmdSnapshotExecuteAction(object obj)
		{
			try
			{
				var fn = Path.GetFileNameWithoutExtension(Filename);
				if (string.IsNullOrEmpty(fn))
				{
					MessageBox.Show("Meeh. filename empty");
					return;
				}

				var path = Path.GetDirectoryName(Filename);
				if (string.IsNullOrEmpty(path))
				{
					MessageBox.Show("Meeh. file path empty");
					return;
				}


				fn = $"{fn}-{DateTime.Now:yyyyMMdd-HHmmss}.png";
				var dest = Path.Combine(path, fn);

				var frameImage = mTemplateData.GetSnapshotBytes(1920, 1080, false, ImageFormatType.kImageFormatPng);
				if (frameImage == null)
					throw new ControlUnexpectedException("GetSnapshotBytes returned null");
				if (frameImage.Data == null)
					throw new ControlUnexpectedException("GetSnapshotBytes frameImage.Data null");

				mLogger.LogAddDebug($"GetSnapshotBytes done. length={frameImage.Data.Length} {frameImage.Width}x{frameImage.Height}");

				File.WriteAllBytes(dest, frameImage.Data);
			}
			catch (Exception e)
			{
				mLogger.LogCatch(nameof(CmdSnapshotExecuteAction), e);
			}
		}
		#endregion

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

			mTemplateData.Touch();
		}
	}
}
