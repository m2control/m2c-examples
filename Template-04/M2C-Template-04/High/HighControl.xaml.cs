﻿using System.Windows;
using System.Windows.Controls;

namespace M2C.Example.Template.High
{
	/// <summary>
	/// Interaction logic for TemplateControl.xaml
	/// </summary>
	internal partial class HighControl : UserControl
	{
		public static readonly DependencyProperty ModelProperty = DependencyProperty.Register(
			"Model", typeof(HighViewModel), typeof(HighControl), new PropertyMetadata(default(HighViewModel)));

		public HighViewModel Model
		{
			get { return (HighViewModel) GetValue(ModelProperty); }
			set { SetValue(ModelProperty, value); }
		}

		public HighControl()
		{
			InitializeComponent();
		}
	}
}
