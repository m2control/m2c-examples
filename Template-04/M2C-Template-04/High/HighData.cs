﻿using System.Collections.Generic;
using System.Windows.Controls;
using MM.M2Control.Interface;
using MM.M2Control.Ventuz;

namespace M2C.Example.Template.High
{
	/// <summary>
	/// template data inherits from a template base class to allow usage of the related device
	/// capabilities. override methods of the base class to have full control over sending data
	/// to a device. the base class gets accessible by referencing the M2Control[Vizrt|...] dll.
	/// </summary>
	class HighData : VentuzTemplateDataBase
	{
		private readonly HighViewModel mModel;

		/// <summary>
		/// this when playlist items get initialized for example opening the playlist
		/// where it is used. the initBlock allows the template to access the interfaces
		/// provided by the client.
		/// </summary>
		/// <param name="initBlock"></param>
		/// <param name="template"></param>
		public HighData(ITemplateDataInitBlock initBlock, ITemplatePlugin template) 
			: base(initBlock, template)
		{
			mModel = new HighViewModel(initBlock.PluginHost.LoggerHandler);
		}

		/// <summary>
		/// return the control for this template - in this case its an wpf control bound
		/// to our view model
		/// </summary>
		/// <returns></returns>
		public override UserControl BuildGui()
		{
			return new HighControl { Model = mModel };
		}

		/// <summary>
		/// create template json that can be cued directly by ventuz.
		/// clipId is the scene referenced scene/template
		/// </summary>
		public override string GetCommandPayload(TransportCommandType commandType, string clipID)
		{
			if (commandType != TransportCommandType.MediaPreparePlay)
				return null;

			// the data we want to send to ventuz
			var dic = new Dictionary<string, string>
			{
				{ "Header", mModel.Header },
				{ "Message", mModel.Message }
			};

			// create the json template structure
			var json = VentuzPayloadBuilder.BuildTemplatePayload(clipID + "/Animation/In", dic);

			/*
			{
				"@": "ventuz://templates/clipID/Scene",
				"Header": null,
				"Message": null
			}
			*/

			return json;
		}

		public override void TakeOut()
		{
			var data = new VentuzPayloadBase
			{
				Scene = "ventuz://templates/scene/template/OUT"
			};

			SendPayload(data.Scene, data);
		}
	}
}
