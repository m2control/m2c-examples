﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template.High
{
	/// <summary>
	/// these exports are used by M2Control to identify the template and its capabilities.
	/// here you define the device that the template can control and to which PluginDll it
	/// belongs. ensure that the TemplateGuid is unique in the system (VS->Tools->Create GUID).
	/// </summary>
	[Export(typeof(ITemplatePlugin))]
	[ExportMetadata(kTemplatePluginDllGuid, PluginDll.cGuid)]
	[ExportMetadata(kTemplateGuid, "981004A1-59BE-4B8E-93D5-C009EF71F83F")]
	[ExportMetadata(kTemplateName, "High Ventuz Template")]
	[ExportMetadata(kTemplateDescription, "A playlist template implementation using the Ventuz template data")]
	[ExportMetadata(kTemplateInstanceRule, TemplateInstanceRuleType.MultipleInPlaylist)]
	[ExportMetadata(kTemplateDeviceHandler, KnownDevices.kVentuzDeviceHandlerGuid)]
	[ExportMetadata(kTemplateDefaultScene, "ventuz://templates/Example04_TemplateData")]
	class HighPlugin : TemplatePluginBase
	{
		// be careful, constructor is called immediately on dll load, before everything else!
		public override ITemplateData InitializeTemplateData(ITemplateDataInitBlock initBlock, List<ITemplateDataValue> data)
		{
			return new HighData(initBlock, this);			
		}
	}
}
