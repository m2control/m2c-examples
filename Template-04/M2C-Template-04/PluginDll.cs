﻿using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template
{
	/// <summary>
	/// this class exports are used by M2Control to identify the plugin as M2Control template.
	/// Only one PluginDll is supported per project. One PluginDll can support multiple templates.
	/// Ensure that the PluginDllGuid is unique in the system (VS->Tools->Create GUID)
	/// 
	/// Use M2CBundle.exe to create a plugin archive (tgz) that can easily transferred and
	/// imported into M2Control.
	/// 
	/// </summary>
	[Export(typeof(ITemplatePluginDll))]
	[ExportMetadata(kPluginDllGuid, cGuid)]
	[ExportMetadata(kPluginDllName, "M2C Example 04")]
	[ExportMetadata(kPluginDllDesc, "Example implementation of ventuz templates (low|high)")]
	[ExportMetadata(kPluginDllGroup, "M2C Examples")]
	[ExportMetadata(kPluginDllIcon, "appbar_ventuz")]
	public class PluginDll : TemplatePluginDllBase
	{		
		public const string cGuid = "AB0526DA-9E33-4D02-BDD8-02E4BFB21635";
	}
}
