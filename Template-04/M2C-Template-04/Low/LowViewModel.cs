﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using MM.M2Control.Interface;
using MM.M2Control.Wpf;

namespace M2C.Example.Template.Low
{
	/// <summary>
	/// model is used separate the data and ui from the template data.
	/// this way it's easy to reuse it to support another device.
	/// </summary>
	class LowViewModel : INotifyPropertyChanged
	{
		private readonly ILogger mLogger;

		private string mHeader;
		private string mMessage;

		/// <summary>
		/// use dependency injection to pass the host logger to the view model
		/// </summary>
		/// <param name="logger"></param>
		public LowViewModel(ILogger logger)
		{
			mLogger = logger;

			// we can bind commands to the control to keep functionality inside
			// the model. that way the control can easily be replaced or changed
			// without modification of code behind file
			CmdReset = new DelegateCommand(CmdResetExecuteAction, CmdResetCanExecute);
		}


		#region Properties

		/// <summary>
		/// just a text property the user can change in the user interface
		/// </summary>
		public string Header
		{
			get { return mHeader; }
			set { mHeader = value; OnPropertyChanged(); }
		}

		/// <summary>
		/// another text property the user can change in the user interface
		/// </summary>
		public string Message
		{
			get { return mMessage; }
			set { mMessage = value; OnPropertyChanged(); }
		}

		#endregion

		#region Commands

		public ICommand CmdReset { get;  }

		/// <summary>
		/// this is called on ui refresh to define the state of the command
		/// returning false will disable the button
		/// </summary>
		/// <param name="arg"></param>
		/// <returns></returns>
		private bool CmdResetCanExecute(object arg)
		{
			return true;
		}

		/// <summary>
		/// the command gets executed. here we just reset the text properties.
		/// because they will fire property change events on change the ui
		/// will be updated immediately
		/// </summary>
		/// <param name="obj"></param>
		private void CmdResetExecuteAction(object obj)
		{
			try
			{
				Header = Message = string.Empty;				
			}
			catch (Exception e)
			{
				mLogger.LogCatch(nameof(CmdResetExecuteAction), e);
			}
		}

		#endregion

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
