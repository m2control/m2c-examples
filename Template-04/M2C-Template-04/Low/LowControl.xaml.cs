﻿using System.Windows;
using System.Windows.Controls;

namespace M2C.Example.Template.Low
{
	/// <summary>
	/// Interaction logic for TemplateControl.xaml
	/// </summary>
	internal partial class LowControl : UserControl
	{
		public static readonly DependencyProperty ModelProperty = DependencyProperty.Register(
			"Model", typeof(LowViewModel), typeof(LowControl), new PropertyMetadata(default(LowViewModel)));

		public LowViewModel Model
		{
			get { return (LowViewModel) GetValue(ModelProperty); }
			set { SetValue(ModelProperty, value); }
		}

		public LowControl()
		{
			InitializeComponent();
		}
	}
}
