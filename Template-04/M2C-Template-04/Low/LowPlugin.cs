﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template.Low
{
	/// <summary>
	/// these exports are used by M2Control to identify the template and its capabilities.
	/// here you define the device that the template can control and to which PluginDll it
	/// belongs. ensure that the TemplateGuid is unique in the system (VS->Tools->Create GUID).
	/// </summary>
	[Export(typeof(ITemplatePlugin))]
	[ExportMetadata(kTemplatePluginDllGuid, PluginDll.cGuid)]
	[ExportMetadata(kTemplateGuid, "05D5AD5C-9D6F-438C-9052-0EBB7D77319E")]
	[ExportMetadata(kTemplateName, "Low Ventuz Template")]
	[ExportMetadata(kTemplateDescription, "A playlist template implementation using the Ventuz Remoting api")]
	[ExportMetadata(kTemplateDeviceHandler, KnownDevices.kVentuzDeviceHandlerGuid)]
	[ExportMetadata(kTemplateDefaultScene, "Example04_Low")]
	[ExportMetadata(kTemplateDeviceControlMode, ControlModeType.LowLevel)] // this one decides!
	class LowPlugin : TemplatePluginBase
	{
		// be careful, constructor is called immediately on dll load, before everything else!
		public override ITemplateData InitializeTemplateData(ITemplateDataInitBlock initBlock, List<ITemplateDataValue> data)
		{
			return new LowData(initBlock, this);			
		}
	}
}
