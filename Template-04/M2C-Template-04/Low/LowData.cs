﻿using System.Windows.Controls;
using MM.M2Control.Interface;
using MM.M2Control.Ventuz;
using MM.M2Control.Ventuz.Entities;

namespace M2C.Example.Template.Low
{
	/// <summary>
	/// template data inherits from a template base class to allow usage of the related device
	/// capabilities. override methods of the base class to have full control over sending data
	/// to a device. the base class gets accessible by referencing the M2Control[Vizrt|...] dll.
	/// </summary>
	class LowData : VentuzTemplateDataBase
	{
		private readonly LowViewModel mModel;

		/// <summary>
		/// this when playlist items get initialized for example opening the playlist
		/// where it is used. the initBlock allows the template to access the interfaces
		/// provided by the client.
		/// </summary>
		/// <param name="initBlock"></param>
		/// <param name="template"></param>
		public LowData(ITemplateDataInitBlock initBlock, ITemplatePlugin template) 
			: base(initBlock, template)
		{
			mModel = new LowViewModel(initBlock.PluginHost.LoggerHandler);
		}

		/// <summary>
		/// return the control for this template - in this case its an wpf control bound
		/// to our view model
		/// </summary>
		/// <returns></returns>
		public override UserControl BuildGui()
		{
			return new LowControl { Model = mModel };
		}

		/// <summary>
		/// here we create the commands to set exposed properties of a Ventuz scene.
		/// attention! you have to put "." in front of the property name.
		/// </summary>
		public override void Take()
		{
			{
				var setprop = new VentuzSetValueCommand(".Header", mModel.Header);
				var command = VentuzCommandSerializer.Serialize(setprop);
				SendCommand(command);
			}

			{
				var setprop = new VentuzSetValueCommand(".Message", mModel.Message);
				var command = VentuzCommandSerializer.Serialize(setprop);
				SendCommand(command);
			}

			// to send special data like color info to ventuz use VentuzMacros
		}
	}
}
