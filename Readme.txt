Template-01:
+ basic implementation of a single playlist template for VizEngine

Template-02:
+ basic implementation of a single playlist template for VizEngine 
+ store/load playlist template data values
+ use global variables as configuration storage
+ use an external DataSource (REST, Database, ..)

Template-03:
+ basic implementation of multiple playlist templates for VizEngine
+ global playout template 
! generate a playlist in a global template

Template-04:
+ basic implementation of Ventuz template using Remoting4 (low) 
+ basic implementation of Ventuz template using TemplateEngine (high) 
! handle take out for both missing

Template-05:
! take snapshots on VizEngine (copy from VizBasicTemplate)
! use different layers on VizEngine (copy from VizBasicTemplate)
! preview graphics on VizEngine (copy from VizBasicTemplate)

Template-06:
! read Icons from M2ControlWpf IconResources.xaml
! compare them with TemplateIconType enum and show the results;
! copy by <Ctrl>C keys of selected Icons to Clipboard
