﻿using System.Collections.Generic;
using System.Windows.Controls;
using MM.M2Control.Interface;
using MM.M2Control.Vizrt;

namespace M2C.Example.Template
{
	/// <summary>
	/// template data inherits from a template base class to allow usage of the related device
	/// capabilities. override methods of the base class to have full control over sending data
	/// to a device. the base class gets accessible by referencing the M2Control[Vizrt|...] dll.
	/// </summary>
	class TemplateData : VizTemplateDataBase
	{
		private readonly TemplateViewModel mModel;

		/// <summary>
		/// this when playlist items get initialized for example opening the playlist
		/// where it is used. the initBlock allows the template to access the interfaces
		/// provided by the client.
		/// </summary>
		/// <param name="initBlock"></param>
		/// <param name="template"></param>
		public TemplateData(ITemplateDataInitBlock initBlock, ITemplatePlugin template) 
			: base(initBlock, template)
		{
			// create an instance of our viewmodel that holds our data and command logic
			mModel = new TemplateViewModel(initBlock.PluginHost.LoggerHandler);
		}

		/// <summary>
		/// return the control for this template - in this case its an wpf control bound
		/// to our view model
		/// </summary>
		/// <returns></returns>
		public override UserControl BuildGui()
		{
			return new TemplateControl { Model = mModel };
		}

		/// <summary>
		/// this is the callback from playlist item take so we use
		/// it to send our model data to the scene and start an animation		
		/// </summary>
		public override void Take()
		{
			// you can use VizMacros to build commands or just add the
			// command string directly to the list
			var commands = new List<string>
			{
				//VizMacros.SetControlPluginValue("header", mModel.Header),
				//VizMacros.SetControlPluginValue("message", mModel.Message),
				VizMacros.SetText("$TEXT$TRANS$header$text", mModel.Header),
				VizMacros.SetText("$TEXT$TRANS$message$text", mModel.Message),
				VizMacros.StartStage()
			};

			SendCommands(commands);
		}

		/// <summary>
		/// because our template has multiple stop points we get a callback
		/// on playlist item continue
		/// </summary>
		public override void TakeContinue()
		{			
			SendCommand(VizMacros.ContinueStage());
		}

		public override void TakeOut()
		{
			SendCommand(VizMacros.StopStage());
		}
	}
}
