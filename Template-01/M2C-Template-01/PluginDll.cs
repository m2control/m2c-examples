﻿using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template
{
	/// <summary>
	/// this class exports are used by M2Control to identify the plugin as M2Control template.
	/// Only 1 PluginDll is supported within 1 project. A single PluginDll can support multiple templates.
	/// Ensure that the PluginDllGuid is unique in the system (VS->Tools->Create GUID)
	/// 
	/// Use M2CBundle.exe to create a plugin archive (tgz) that can easily transferred and
	/// imported into M2Control.
	/// </summary>
	[Export(typeof(ITemplatePluginDll))]
	[ExportMetadata(kPluginDllGuid, cGUID)]
	[ExportMetadata(kPluginDllName, "M2C Example 01")]
	[ExportMetadata(kPluginDllDesc, "Example template for basic template implementation")]	
	[ExportMetadata(kPluginDllGroup, "M2C Examples")]	
	public class PluginDll : TemplatePluginDllBase
	{				
		public const string cGUID = "440AD6A4-AF91-4029-A67A-85DC1D87AAEF";		
	}
}
