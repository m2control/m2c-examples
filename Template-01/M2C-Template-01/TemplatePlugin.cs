﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template
{
	/// <summary>
	/// These exports are used by M2Control to identify the template and its capabilities.
	/// Here you define the device that the template can control and to which PluginDll it
	/// belongs. Ensure that the TemplateGuid is unique in the system (VS->Tools->Create GUID).
	/// </summary>
	[Export(typeof(ITemplatePlugin))]
	[ExportMetadata(kTemplatePluginDllGuid, PluginDll.cGUID)]
	[ExportMetadata(kTemplateGuid, "43D434B7-231B-4FE5-8083-2D9EB77564AD")]	
	[ExportMetadata(kTemplateName, "Playlist Template 01")]
	[ExportMetadata(kTemplateDescription, "A basic playlist template implementation")]
	
	// define if the template can be used multiple times inside a playlist
	[ExportMetadata(kTemplateInstanceRule, TemplateInstanceRuleType.MultipleInPlaylist)]
	
	// define which device the template wants to control
	[ExportMetadata(kTemplateDeviceHandler, KnownDevices.kVizEngineDeviceHandlerGuid)]

	// define the default scene path the template uses
	[ExportMetadata(kTemplateDefaultScene, "SCENE*Examples/TwoTexts")]
	class TemplatePlugin : TemplatePluginBase
	{
		// be careful, constructor is called immediately on dll load, before everything else!
		public override ITemplateData InitializeTemplateData(ITemplateDataInitBlock initBlock, List<ITemplateDataValue> data)
		{
			return new TemplateData(initBlock, this);			
		}
	}
}
