﻿using System.Windows;
using System.Windows.Controls;

namespace M2C.Example.Template.SendText
{
	/// <summary>
	/// Interaction logic for TemplateControl.xaml
	/// </summary>
	internal partial class SendTextControl : UserControl
	{
		public static readonly DependencyProperty ModelProperty = DependencyProperty.Register(
			"Model", typeof(SendTextViewModel), typeof(SendTextControl), new PropertyMetadata(default(SendTextViewModel)));

		public SendTextViewModel Model
		{
			get { return (SendTextViewModel) GetValue(ModelProperty); }
			set { SetValue(ModelProperty, value); }
		}

		public SendTextControl()
		{
			InitializeComponent();
		}
	}
}
