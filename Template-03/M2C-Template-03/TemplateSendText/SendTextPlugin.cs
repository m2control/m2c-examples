﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template.SendText
{
	/// <summary>
	/// these exports are used by M2Control to identify the template and its capabilities.
	/// here you define the device that the template can control and to which PluginDll it
	/// belongs. ensure that the TemplateGuid is unique in the system (VS->Tools->Create GUID).
	/// </summary>
	[Export(typeof(ITemplatePlugin))]
	[ExportMetadata(kTemplatePluginDllGuid, PluginDll.cGUID)]
	[ExportMetadata(kTemplateGuid, "4B3ECF91-B3A0-48DE-8DF5-D22FAF8D0150")]	
	[ExportMetadata(kTemplateName, "SendText Viz Template")]
	[ExportMetadata(kTemplateDescription, "A simple template to send a text to viz scene")]
	[ExportMetadata(kTemplateInstanceRule, TemplateInstanceRuleType.MultipleInPlaylist)]
	[ExportMetadata(kTemplateDeviceHandler, KnownDevices.kVizEngineDeviceHandlerGuid)]
	//[ExportMetadata(kTemplateDefaultScene, "")]
	//[ExportMetadata(kTemplateHasVariants, false)]
	[ExportMetadata(kTemplateDefaultAutomationID, "SendText")]
	public class SendTextPlugin : TemplatePluginBase
	{
		// be careful, constructor is called immediately on dll load, before everything else!
		public override ITemplateData InitializeTemplateData(ITemplateDataInitBlock initBlock, List<ITemplateDataValue> data)
		{
			return new SendTextData(initBlock, this, data);
		}
	}
}
