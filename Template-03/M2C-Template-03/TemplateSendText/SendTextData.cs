﻿using System.Collections.Generic;
using System.Windows.Controls;
using MM.M2Control.Interface;
using MM.M2Control.Vizrt;

namespace M2C.Example.Template.SendText
{
	/// <summary>
	/// template data inherits from a template base class to allow usage of the related device
	/// capabilities. override methods of the base class to have full control over sending data
	/// to a device. the base class gets accessible by referencing the M2Control[Vizrt|...] dll.
	/// </summary>
	class SendTextData : VizTemplateDataBase
	{
		private readonly SendTextViewModel mModel;

		private List<string> Scenes = new List<string>
		{
			"SCENE*Examples/Example05_LayerFront",
			"SCENE*Examples/Example05_LayerMiddle",
			"SCENE*Examples/Example05_LayerBack"
		};

		/// <summary>
		/// this when playlist items get initialized for example opening the playlist
		/// where it is used. the initBlock allows the template to access the interfaces
		/// provided by the client.
		/// </summary>
		/// <param name="initBlock"></param>
		/// <param name="template"></param>
		public SendTextData(ITemplateDataInitBlock initBlock, ITemplatePlugin template,
			List<ITemplateDataValue> data)
			: base(initBlock, template)
		{
			mModel = new SendTextViewModel(this, initBlock.PluginHost.LoggerHandler);

			mModel.Layers = new List<string>
			{
				VizMacros.kVizFrontLayer, VizMacros.kVizMiddleLayer, VizMacros.kVizBackLayer
			};
			SetDataValuesInternal(data);
		}

		//public override string InvariantScene { get => ""; }

		/// <summary>
		/// return the control for this template - in this case its an wpf control bound
		/// to our view model
		/// </summary>
		/// <returns></returns>
		public override UserControl BuildGui()
		{
			return new SendTextControl { Model = mModel };
		}
		
		/// <summary>
		/// this is called on save an opened playlist template
		/// </summary>
		/// <returns></returns>
		public override List<ITemplateDataValue> GetDataValues()
		{
			return mModel.GetDataValues(PluginHost.Factory);			
		}

		/// <summary>
		/// template data was changed so m2c will pass changes here
		/// </summary>
		/// <param name="data"></param>
		public override void SetDataValues(List<ITemplateDataValue> data)
		{
			SetDataValuesInternal(data);
		}

		private void SetDataValuesInternal(List<ITemplateDataValue> data)
		{
			if (data == null)
				return;

			mModel.SetDataValues(this, data);
		}

		#region Control Graphics

		public override int ContinueCount => string.IsNullOrEmpty(mModel.TextField) ? 1 : 2;

		public override void PrepareTake()
		{
			// load scene
			var i = mModel.Layers.IndexOf(mModel.Layer);
			if (i >= 0)
				SendCommand($"RENDERER*{mModel.Layer} SET_OBJECT {Scenes[i]}");
		}

		/// <summary>
		/// this is the callback from playlist item take so we use
		/// it to send our model data to the scene and start an animation		
		/// </summary>
		public override void Take()
		{
			// you can use VizMacros to build commands or just add the
			// command string directly to the list

			mModel.OnTake();

			SendCommand($"RENDERER*{mModel.Layer}*STAGE START");
		}

		/// <summary>
		/// because our template has multiple stop points we get a callback
		/// on playlist item continue
		/// </summary>
		public override void TakeContinue()
		{
			SendCommand($"RENDERER*{mModel.Layer}*STAGE CONTINUE");
		}

		public override void TakeOut()
		{
			SendCommand($"RENDERER*{mModel.Layer}*STAGE STOP");
		}

		public void SendButtonMessageEx(string lr, string text)
		{
			SendCommand("RENDERER*" + lr + "*TREE*$TEXT$TRANS$txt_1$text*GEOM*TEXT SET " + text);
		}
		#endregion
	}
}
