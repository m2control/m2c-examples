﻿using System.Windows;
using System.Windows.Controls;

namespace M2C.Example.Template.Global
{
	/// <summary>
	/// Interaction logic for TemplateControl.xaml
	/// </summary>
	internal partial class GlobalControl : UserControl
	{
		public static readonly DependencyProperty ModelProperty = DependencyProperty.Register(
			"Model", typeof(GlobalViewModel), typeof(GlobalControl), new PropertyMetadata(default(GlobalViewModel)));

		public GlobalViewModel Model
		{
			get { return (GlobalViewModel) GetValue(ModelProperty); }
			set { SetValue(ModelProperty, value); }
		}

		public GlobalControl()
		{
			InitializeComponent();
		}
	}
}
