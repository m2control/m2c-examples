﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template.Global
{
	/// <summary>
	/// kTemplateInstanceRule set to Global will prevent adding this template to playlist.
	/// if there's a MenuGroup defined it gets added to the M2Control main menu. Global
	/// templates can also be opened in playout area and can stored within te layout.
	/// </summary>
	[Export(typeof(ITemplatePlugin))]
	[ExportMetadata(kTemplatePluginDllGuid, PluginDll.cGUID)]
	[ExportMetadata(kTemplateGuid, "548C885C-6B9A-4E1E-96B8-801FBB147D40")]	
	[ExportMetadata(kTemplateName, "Global Template")]
	[ExportMetadata(kTemplateDescription, "A basic playlist template implementation")]
	
	[ExportMetadata(kTemplateInstanceRule, TemplateInstanceRuleType.Global)]
	[ExportMetadata(kTemplateDeviceHandler, KnownDevices.kVizEngineDeviceHandlerGuid)]
	[ExportMetadata(kTemplateMenuGroup, "M2C Examples")]
	[ExportMetadata(kTemplateIcon, TemplateIconType.CloudDownload)]
	public class GlobalPlugin : TemplatePluginBase
	{
		// be careful, constructor is called immediately on dll load, before everything else!
		public override ITemplateData InitializeTemplateData(ITemplateDataInitBlock initBlock, List<ITemplateDataValue> data)
		{
			return new GlobalData(initBlock, this);			
		}
	}
}

