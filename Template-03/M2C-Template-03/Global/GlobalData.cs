﻿using System.Windows.Controls;
using MM.M2Control.Interface;
using MM.M2Control.Vizrt;

namespace M2C.Example.Template.Global
{
	/// <summary>
	/// template data inherits from a template base class to allow usage of the related device
	/// capabilities. override methods of the base class to have full control over sending data
	/// to a device. the base class gets accessible by referencing the M2Control[Vizrt|...] dll.
	/// </summary>
	class GlobalData : VizTemplateDataBase
	{
		private readonly GlobalViewModel mModel;

		/// <summary>
		/// this when playlist items get initialized for example opening the playlist
		/// where it is used. the initBlock allows the template to access the interfaces
		/// provided by the client.
		/// </summary>
		/// <param name="initBlock"></param>
		/// <param name="template"></param>
		public GlobalData(ITemplateDataInitBlock initBlock, ITemplatePlugin template) 
			: base(initBlock, template)
		{
			mModel = new GlobalViewModel(this, initBlock.PluginHost.LoggerHandler);
		}

		public override void LoadData()
		{
			// get the viz layers
			mModel.DeviceLayers = VizControllerMacros.GetAllDeviceLayerItems(PluginHost.Factory);
			mModel.OnLoadLayers();
		}


		/// <summary>
		/// return the control for this template - in this case its an wpf control bound
		/// to our view model
		/// </summary>
		/// <returns></returns>
		public override UserControl BuildGui()
		{
			return new GlobalControl { Model = mModel };
		}		
	}
}
