﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using MM.M2Control.Interface;
using MM.M2Control.Vizrt;
using MM.M2Control.Wpf;

namespace M2C.Example.Template.Global
{
	/// <summary>
	/// model is used separate the data and ui from the template data.
	/// this way it's easy to reuse it to support another device.
	/// </summary>
	class GlobalViewModel : INotifyPropertyChanged
	{
		private readonly ILogger mLogger;
		private readonly GlobalData mTemplateData;
		private string mScenePath;
		private IDeviceLayerItem[] mDeviceLayers;
		private IDeviceLayerItem mSelectedDeviceLayer;


		/// <summary>
		/// use dependency injection to pass the templateData and host logger to the view model
		/// </summary>
		/// <param name="templateData"></param>
		/// <param name="logger"></param>
		public GlobalViewModel(GlobalData templateData, ILogger logger)
		{
			mTemplateData = templateData;
			mLogger = logger;

			// we can bind commands to the control to keep functionality inside
			// the model. that way the control can easily be replaced or changed
			// without changed to controls code behind file
			CmdLoadScene = new DelegateCommand(CmdLoadSceneExecuteAction, CmdLoadSceneCanExecute);
			CmdClearScene = new DelegateCommand(CmdClearSceneExecuteAction, CmdClearSceneCanExecute);

			mScenePath = "SCENE*Examples/Example05_LayerMiddle";
		}


		#region Properties

		public string ScenePath
		{
			get { return mScenePath; }
			set { mScenePath = value; OnPropertyChanged(); }
		}

		public IDeviceLayerItem[] DeviceLayers
		{
			get { return mDeviceLayers; }
			set { mDeviceLayers = value; OnPropertyChanged(); }
		}

		public IDeviceLayerItem SelectedDeviceLayer
		{
			get { return mSelectedDeviceLayer; }
			set { mSelectedDeviceLayer = value; OnPropertyChanged();}
		}

		#endregion

		#region Methods

		public void OnLoadLayers()
		{
			// preselect first
			SelectedDeviceLayer = DeviceLayers.FirstOrDefault();
		}

		#endregion

		#region Commands

		public ICommand CmdLoadScene { get;  }

		public ICommand CmdClearScene { get;  }
		
		/// <summary>
		/// this is called on ui refresh to define the state of the command
		/// returning false will disable the button
		/// </summary>
		/// <param name="arg"></param>
		/// <returns></returns>
		private bool CmdLoadSceneCanExecute(object arg)
		{
			return !string.IsNullOrEmpty(ScenePath);
		}

		/// <summary>
		/// the command gets executed. here we just reset the text properties
		/// and combobox selected item. because they will fire property change
		/// events on change the ui will be updated immediately
		/// </summary>
		/// <param name="obj"></param>
		private void CmdLoadSceneExecuteAction(object obj)
		{
			try
			{
				// use template data to send commands to selected layer 
				// this will throw if there's no matching device (here viz) is available
				mTemplateData.SendCommand(SelectedDeviceLayer.Key, VizMacros.SetRendererScene(ScenePath));
				mTemplateData.SendCommand(SelectedDeviceLayer.Key, VizMacros.StartStage());
			}
			catch (Exception e)
			{
				mLogger.LogCatch(nameof(CmdLoadSceneExecuteAction), e);
			}
		}

		private bool CmdClearSceneCanExecute(object arg)
		{
			return true;
		}

		
		private void CmdClearSceneExecuteAction(object obj)
		{
			try
			{
				mTemplateData.SendCommand(SelectedDeviceLayer.Key, VizMacros.SetRendererScene(string.Empty));
			}
			catch (Exception e)
			{
				mLogger.LogCatch(nameof(CmdLoadSceneExecuteAction), e);
			}
		}
		

		#endregion

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

	}
}
