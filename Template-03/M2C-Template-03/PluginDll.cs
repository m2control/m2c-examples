﻿using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template
{
	/// <summary>
	/// this class exports are used by M2Control to identify the plugin as M2Control template.
	/// Only one PluginDll is supported per project. One PluginDll can support multiple templates.
	/// Ensure that the PluginDllGuid is unique in the system (VS->Tools->Create GUID)
	/// 
	/// Use M2CBundle.exe to create a plugin archive (tgz) that can easily transferred and
	/// imported into M2Control.
	/// 
	/// </summary>
	[Export(typeof(ITemplatePluginDll))]
	[ExportMetadata(kPluginDllGuid, cGUID)]
	[ExportMetadata(kPluginDllName, "M2C Example 03")]
	[ExportMetadata(kPluginDllDesc, "Example implementation of multiple playlist templates and a global one")]
	[ExportMetadata(kPluginDllGroup, "M2C Examples")]
	[ExportMetadata(kPluginDllIcon, "appbar_puzzle_round")]
	public class PluginDll : TemplatePluginDllBase
	{		
		public const string cGUID = "846A621F-58D7-4641-B355-6F2B625D43CF";		
	}
}
