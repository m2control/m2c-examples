﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template.PreferredLayer
{
	/// <summary>
	/// these exports are used by M2Control to identify the template and its capabilities.
	/// here you define the device that the template can control and to which PluginDll it
	/// belongs. ensure that the TemplateGuid is unique in the system (VS->Tools->Create GUID).
	/// </summary>
	[Export(typeof(ITemplatePlugin))]
	[ExportMetadata(kTemplatePluginDllGuid, PluginDll.cGUID)]
	[ExportMetadata(kTemplateGuid, "0F032038-7A12-4A0C-AC6F-E8F6E2132836")]	
	[ExportMetadata(kTemplateName, "PreferredLayer Viz Template")]
	[ExportMetadata(kTemplateDescription, "A simple template to send a text to viz scene")]
	[ExportMetadata(kTemplateInstanceRule, TemplateInstanceRuleType.MultipleInPlaylist)]
	[ExportMetadata(kTemplateDeviceHandler, KnownDevices.kVizEngineDeviceHandlerGuid)]
	[ExportMetadata(kTemplateDefaultScene, "SCENE*Examples/Example05_LayerBack")]
	[ExportMetadata(kTemplateDefaultAutomationID, "PreferredLayer")]
	[ExportMetadata(kTemplatePreferredVizLayer, "BACK_LAYER")]
	public class PreferredLayerPlugin : TemplatePluginBase
	{
		// be careful, constructor is called immediately on dll load, before everything else!
		public override ITemplateData InitializeTemplateData(ITemplateDataInitBlock initBlock, List<ITemplateDataValue> data)
		{
			return new PreferredLayerData(initBlock, this, data);
		}
	}
}
