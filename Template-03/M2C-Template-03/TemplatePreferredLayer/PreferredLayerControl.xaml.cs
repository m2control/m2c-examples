﻿using System.Windows;
using System.Windows.Controls;

namespace M2C.Example.Template.PreferredLayer
{
	/// <summary>
	/// Interaction logic for TemplateControl.xaml
	/// </summary>
	internal partial class PreferredLayerControl : UserControl
	{
		public static readonly DependencyProperty ModelProperty = DependencyProperty.Register(
			"Model", typeof(PreferredLayerViewModel), typeof(PreferredLayerControl), new PropertyMetadata(default(PreferredLayerViewModel)));

		public PreferredLayerViewModel Model
		{
			get { return (PreferredLayerViewModel) GetValue(ModelProperty); }
			set { SetValue(ModelProperty, value); }
		}

		public PreferredLayerControl()
		{
			InitializeComponent();
		}
	}
}
