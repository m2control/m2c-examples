﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using M2C.Example.Database;
using MM.M2Control.Interface;

namespace M2C.Example.Template
{
	/// <summary>
	/// we also want to use a database now - of course you can directly open a connection to any
	/// database in your model class. but you'll probably want to have editable parameters
	/// for your database connection. maybe you want to display the status of the database too?
	/// the extensions to PluginDll will show how to do this.
	/// 
	/// </summary>
	[Export(typeof(ITemplatePluginDll))]
	[ExportMetadata(kPluginDllGuid, cGUID)]
	[ExportMetadata(kPluginDllName, "M2C Example 02")]
	[ExportMetadata(kPluginDllDesc, "Example template implementation with database support")]
	[ExportMetadata(kPluginDllGroup, "M2C Examples")]
	[ExportMetadata(kPluginDllIcon, "appbar_database")]
	public class PluginDll : TemplatePluginDllBase
	{		
		public const string cGUID = "C07AA703-AA6F-4B65-B382-018F31343F5C";

		private IPluginDataSourceStatus mDatabaseStatus;  

		public override IReadOnlyList<IPluginDataSourceStatus> GetDataSources()
		{
			var pluginGuid = new Guid(cGUID);

			// create status item to be displayed on the status bar
			// add a callback that gets executed on startup (AutoInitialize==true)
			// or when user clicks on the status icon
			if (mDatabaseStatus == null)
			{
				mDatabaseStatus = PluginHost.Factory.CreateDataSourceStatus(pluginGuid, InitializeMainDb);
				mDatabaseStatus.Title = "Template-02 DB";
				mDatabaseStatus.Description = string.Format("Initialize {0} Database", PluginDllName);
				mDatabaseStatus.SourceType = PluginDataSourceType.Database;
				mDatabaseStatus.AutoInitialize = true;
			}

			return new List<IPluginDataSourceStatus>
			{
				mDatabaseStatus
			};
		}

		/// <summary>
		/// will reload the global settings from hash because there could be changes
		/// </summary>
		private void InitializeMainDb(IPluginDataSourceParameter p)
		{
			var settings = new TemplateSettings();
			settings.Load(PluginHost);
			
			mDatabaseStatus.Connection = settings.DatabaseConnection;			
			
			// throw if something goes wrong - error will be displayed on status icon
			var database = DatabaseFactory.CreateDatabaseInstance(mDatabaseStatus.Connection);
			
			mDatabaseStatus.Description = string.Format("{0} :{1} - Settings({2}) OK{1}",
				mDatabaseStatus.Title, Environment.NewLine, TemplateSettings.cHashConfigKey);
		}

		public override void TerminatePluginDll()
		{
			// might disconnect query log
			DatabaseFactory.Cleanup();
		}
	}
}
