﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template
{
	/// <summary>
	/// these exports are used by M2Control to identify the template and its capabilities.
	/// here you define the device that the template can control and to which PluginDll it
	/// belongs. ensure that the TemplateGuid is unique in the system (VS->Tools->Create GUID).
	/// </summary>
	[Export(typeof(ITemplatePlugin))]
	[ExportMetadata(kTemplatePluginDllGuid, PluginDll.cGUID)]
	[ExportMetadata(kTemplateGuid, "A97DCBCD-7C91-4CB8-8E31-60CEA3B58BEF")]	
	[ExportMetadata(kTemplateName, "Playlist Template 02")]
	[ExportMetadata(kTemplateDescription, "A basic playlist template implementation")]
	[ExportMetadata(kTemplateInstanceRule, TemplateInstanceRuleType.MultipleInPlaylist)]
	[ExportMetadata(kTemplateDeviceHandler, KnownDevices.kVizEngineDeviceHandlerGuid)]
	[ExportMetadata(kTemplateDefaultScene, "SCENE*Examples/ThreeTexts")]
	class TemplatePlugin : TemplatePluginBase
	{
		// be careful, constructor is called immediately on dll load, before everything else!
		public override ITemplateData InitializeTemplateData(ITemplateDataInitBlock initBlock, List<ITemplateDataValue> data)
		{
			return new TemplateData(initBlock, this, data);			
		}
	}
}
