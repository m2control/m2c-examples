﻿using System.Reflection;
using System.Runtime.InteropServices;
using MM.M2Control;

[assembly: AssemblyTitle("M2Control Template-02")]
[assembly: AssemblyProduct("M2Control Template Plugin")]
[assembly: AssemblyDescription("Contains an example template implementation")]

[assembly: AssemblyCompany(M2ControlInfo.M2ControlTeam)]
[assembly: AssemblyCopyright(M2ControlInfo.M2ControlCopyright)]
[assembly: AssemblyVersion(M2ControlInfo.Version)]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

