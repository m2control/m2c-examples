﻿using System.Collections.Generic;
using System.Windows.Controls;
using MM.M2Control.Interface;
using MM.M2Control.Vizrt;

namespace M2C.Example.Template
{
	/// <summary>
	/// template data inherits from a template base class to allow usage of the related device
	/// capabilities. override methods of the base class to have full control over sending data
	/// to a device. the base class gets accessible by referencing the M2Control[Vizrt|...] dll.
	/// </summary>
	class TemplateData : VizTemplateDataBase
	{
		private readonly TemplateViewModel mModel;

		/// <summary>
		/// this when playlist items get initialized for example opening the playlist
		/// where it is used. the initBlock allows the template to access the interfaces
		/// provided by the client.
		/// </summary>
		/// <param name="initBlock">handy classes passed from client to template</param>
		/// <param name="template">an instance of current plugin</param>
		/// <param name="data">data stored for this templates playlist item</param>
		public TemplateData(ITemplateDataInitBlock initBlock, ITemplatePlugin template,
			List<ITemplateDataValue> data) 
			: base(initBlock, template)
		{
			mModel = new TemplateViewModel(this, initBlock.PluginHost.LoggerHandler);

			SetDataValuesInternal(data);
		}


		/// <summary>
		/// return the control for this template - in this case its an wpf control bound
		/// to our view model
		/// </summary>
		/// <returns></returns>
		public override UserControl BuildGui()
		{
			return new TemplateControl { Model = mModel };
		}

		/// <summary>
		/// this is called on opening the playlist or if user requests load manually
		/// </summary>
		public override void LoadData()
		{
			// let our model load needed items from database
			mModel.LoadDatabaseItems();
		}

		/// <summary>
		/// this is called on save an opened playlist template
		/// </summary>
		/// <returns></returns>
		public override List<ITemplateDataValue> GetDataValues()
		{
			return mModel.GetDataValues(PluginHost.Factory);			
		}

		/// <summary>
		/// template data was changed so m2c will pass changes here
		/// </summary>
		/// <param name="data"></param>
		public override void SetDataValues(List<ITemplateDataValue> data)
		{
			SetDataValuesInternal(data);
		}

		private void SetDataValuesInternal(List<ITemplateDataValue> data)
		{
			if (data == null)
				return;

			mModel.SetDataValues(this, data);
		}

		#region Control Graphics

		/// <summary>
		/// this is the callback from playlist item take so we use
		/// it to send our model data to the scene and start an animation		
		/// </summary>
		public override void Take()
		{
			// you can use VizMacros to build commands or just add the
			// command string directly to the list
			var commands = new List<string>
			{
				//VizMacros.SetControlPluginValue("header", mModel.Header),
				//VizMacros.SetControlPluginValue("message", mModel.Message),
				//VizMacros.SetControlPluginValue("item", mModel.SelectedItem?.Title),
				VizMacros.SetText("$TEXT$TRANS$header$text", mModel.Header),
				VizMacros.SetText("$TEXT$TRANS$message$text", mModel.Message),
				VizMacros.SetText("$TEXT$TRANS$title$text", mModel.SelectedItem?.Title),
				VizMacros.StartStage()
			};

			SendCommands(commands);
		}

		/// <summary>
		/// because our template has multiple stop points we get a callback
		/// on playlist item continue
		/// </summary>
		public override void TakeContinue()
		{			
			SendCommand(VizMacros.ContinueStage());
		}

		public override void TakeOut()
		{
			SendCommand(VizMacros.StopStage());
		}

		#endregion
	}
}
