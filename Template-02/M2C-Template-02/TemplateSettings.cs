﻿using System;
using System.Collections.Generic;
using System.Linq;
using MM.M2Control.Interface;

namespace M2C.Example.Template
{
	/// <summary>
	/// this class is used to handle specific template settings. in this example we want to
	/// store the connection string of our database here so it can be modified later without
	/// changing the code.
	/// they can be edited under M2Control->Configuration->Variables
	/// </summary>
	class TemplateSettings
	{
		public const string cHashConfigKey = "Template-02";

		// this is the key of our database connection. first part before the dot is used to group keys
		private const string cDbConnectionKey = "database.connection";
		private const string cDbEnabledKey = "database.enable";

		private static readonly List<Tuple<string, string, string>> sAllSettings =
			new List<Tuple<string, string, string>>
			{
				new Tuple<string, string, string>(cDbConnectionKey, "Data source=host/svc;User ID=xxxx;Password=xxxx;",
					"mandatory"),
				new Tuple<string, string, string>(cDbEnabledKey, "1", "enable=1, disable=0"),
				// add more parameters here if needed...
			};

		/// <summary>
		/// load our configuration key-value pairs (hash) - if it does not exist in the system it gets created using the default values of the tuple.
		/// </summary>
		/// <param name="host"></param>
		public void Load(ITemplatePluginHost host)
		{
			var conf = host.Factory.CreateHashConfig();

			var skeys = sAllSettings.Select(k => k.Item1).ToList();
			var svalues = sAllSettings.Select(k => k.Item2).ToList();

			// load/create our hash storing our key-value pairs there 
			conf.InitConfigHash(host.HashHandler, cHashConfigKey, skeys, svalues, host.GetCurrentUser());

			// read the value of our database key - it would throw if it's empty!
			DatabaseConnection = conf.GetValueS_NonEmpty(cDbConnectionKey);
			DatabaseEnable = conf.GetValueB(cDbEnabledKey);
		}

		#region Properties

		public string DatabaseConnection { get; private set; }

		public bool DatabaseEnable { get; private set; }

		#endregion
	}
}
