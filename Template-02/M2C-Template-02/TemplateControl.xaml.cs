﻿using System.Windows;
using System.Windows.Controls;

namespace M2C.Example.Template
{
	/// <summary>
	/// Interaction logic for TemplateControl.xaml
	/// </summary>
	internal partial class TemplateControl : UserControl
	{
		public static readonly DependencyProperty ModelProperty = DependencyProperty.Register(
			"Model", typeof(TemplateViewModel), typeof(TemplateControl), new PropertyMetadata(default(TemplateViewModel)));

		public TemplateViewModel Model
		{
			get { return (TemplateViewModel) GetValue(ModelProperty); }
			set { SetValue(ModelProperty, value); }
		}

		public TemplateControl()
		{
			InitializeComponent();
		}
	}
}
