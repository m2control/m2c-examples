﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using M2C.Example.Database;
using MM.M2Control.Interface;
using MM.M2Control.Wpf;

namespace M2C.Example.Template
{
	/// <summary>
	/// model is used separate the data and ui from the template data.
	/// this way it's easy to reuse it to support another device.
	/// </summary>
	class TemplateViewModel : INotifyPropertyChanged
	{
		private const string cHeaderKey = "header";
		private const string cMessageKey = "message";
		private const string cItemKey = "item";

		private readonly TemplateDataBase mTemplateData; 
		private readonly ILogger mLogger;

		private string mHeader;
		private string mMessage;
		private List<DatabaseItem> mItems;
		private DatabaseItem mSelectedItem;

		/// <summary>
		/// use dependency injection to pass the templateData and host logger to the view model
		/// </summary>
		/// <param name="templateData"></param>
		/// <param name="logger"></param>
		public TemplateViewModel(TemplateDataBase templateData, ILogger logger)
		{
			mTemplateData = templateData;
			mLogger = logger;

			mTemplateData.ClearModified();

			// we can bind commands to the control to keep functionality inside
			// the model. that way the control can easily be replaced or changed
			// without changed to controls code behind file
			CmdReset = new DelegateCommand(CmdResetExecuteAction, CmdResetCanExecute);
		}


		#region Properties

		/// <summary>
		/// just a text property the user can change in the user interface
		/// </summary>
		public string Header
		{
			get { return mHeader; }
			set
			{
				if (mHeader == value)
					return;

				mHeader = value; 
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// another text property the user can change in the user interface
		/// </summary>
		public string Message
		{
			get { return mMessage; }
			set
			{
				if (mMessage == value)
					return;

				mMessage = value; 
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// a list of database items that we bind to a combobox
		/// </summary>
		public List<DatabaseItem> Items
		{
			get { return mItems; }
			set { mItems = value; OnPropertyChanged(); }
		}

		/// <summary>
		/// the currently selected item of the combobox
		/// </summary>
		public DatabaseItem SelectedItem
		{
			get { return mSelectedItem; }
			set
			{
				if (Equals(mSelectedItem, value))
					return;

				mSelectedItem = value; 
				OnPropertyChanged();
			}
		}

		#endregion

		#region Methods
		
		public void LoadDatabaseItems()
		{
			// get the database instance and
			var db = DatabaseFactory.GetDatabaseInstance();
			
			// load items from our database
			Items = db.QueryDataItems();

			// select first item from the list
			SelectedItem = Items.FirstOrDefault();
		}

		/// <summary>
		/// return a list of values you want to store inside the playlist.
		/// they get stored my m2control if user saves the item and the template data
		/// was modified (touch it!)
		/// </summary>
		/// <param name="factory"></param>
		/// <returns></returns>
		public List<ITemplateDataValue> GetDataValues(IClassFactory factory)
		{
			return new List<ITemplateDataValue>
			{
				factory.CreateTemplateDataValue(cHeaderKey, Header),
				factory.CreateTemplateDataValue(cMessageKey, Message),
				factory.CreateTemplateDataValue(cItemKey, TemplateValueConvert.Stringize(SelectedItem?.Id ?? 0)),
			};
		}

		/// <summary>
		/// set the property values using the data stored in the values list. this is called by m2control
		/// on loading the template if it's contained in a playlist.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="values"></param>		
		public void SetDataValues(TemplateDataBase data, List<ITemplateDataValue> values)
		{			
			Header = data.GetDataValue(values, cHeaderKey, string.Empty);
			Message = data.GetDataValue(values, cMessageKey, string.Empty);

			var itemId = data.GetDataValue(values, cItemKey, 0);
			if (Items != null)
				SelectedItem = Items.FirstOrDefault(k => k.Id == itemId);
		}

		#endregion

		#region Commands

		public ICommand CmdReset { get;  }

		/// <summary>
		/// this is called on ui refresh to define the state of the command
		/// returning false will disable the button
		/// </summary>
		/// <param name="arg"></param>
		/// <returns></returns>
		private bool CmdResetCanExecute(object arg)
		{
			return true;
		}

		/// <summary>
		/// the command gets executed. here we just reset the text properties
		/// and combobox selected item. because they will fire property change
		/// events on change the ui will be updated immediately
		/// </summary>
		/// <param name="obj"></param>
		private void CmdResetExecuteAction(object obj)
		{
			try
			{
				Header = Message = string.Empty;
				SelectedItem = Items.FirstOrDefault();
			}
			catch (Exception e)
			{
				mLogger.LogCatch(nameof(CmdResetExecuteAction), e);
			}
		}

		#endregion

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

			// we need to notify the template that values have changed
			mTemplateData.Touch();
		}
	}
}
