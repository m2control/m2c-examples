﻿using System;

namespace M2C.Example.Database
{
	/// <summary>
	/// class is implementing IEquatable interface now so item controls
	/// can identify which item is selected for example
	/// </summary>
	public class DatabaseItem : IEquatable<DatabaseItem>
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }

		public bool Equals(DatabaseItem other)
		{
			if (other == null)
				return false;

			return Id == other.Id;
		}		
	}
}
