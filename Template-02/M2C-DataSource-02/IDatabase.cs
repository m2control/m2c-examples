﻿using System.Collections.Generic;

namespace M2C.Example.Database
{
	/// <summary>
	/// hide the real database implementation behind an interface
	/// </summary>
	public interface IDatabase
	{
		bool IsConnected { get; }

		string Connection { get; }

		List<DatabaseItem> QueryDataItems();

		DatabaseItem QueryDataItem(int id);
	}
}
