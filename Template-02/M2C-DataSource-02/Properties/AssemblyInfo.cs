﻿using System.Reflection;
using System.Runtime.InteropServices;
using MM.M2Control;

[assembly: AssemblyTitle("M2Control DataSource-01")]
[assembly: AssemblyProduct("M2Control DataSource Library")]
[assembly: AssemblyDescription("Contains an example database library")]

[assembly: AssemblyCompany(M2ControlInfo.M2ControlTeam)]
[assembly: AssemblyCopyright(M2ControlInfo.M2ControlCopyright)]
[assembly: AssemblyVersion(M2ControlInfo.Version)]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]