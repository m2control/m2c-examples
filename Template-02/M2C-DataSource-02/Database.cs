﻿using System.Collections.Generic;
using System.Linq;

namespace M2C.Example.Database
{
	/// <summary>
	/// this is just a test implementation without real database engine implementation.
	/// M2Control ships Oracle.Managed.DataAccess, PostgreSQL so if we link these we can
	/// use the full power of .NET database support.
	/// </summary>
	class Database : IDatabase
	{
		private readonly List<DatabaseItem> mRecords = new List<DatabaseItem>();

		public Database()
		{			
			InitDataItems();
		}

		public void Connect(string connectionString)
		{
			Connection = connectionString;
			IsConnected = true;
		}

		public bool IsConnected { get; private set; }

		public string Connection { get; private set; }

		public List<DatabaseItem> QueryDataItems()
		{
			return mRecords;
		}

		public DatabaseItem QueryDataItem(int id)
		{
			return mRecords.FirstOrDefault(k => k.Id == id);
		}


		private void InitDataItems()
		{
			for (int i = 1; i < 50; i++)
			{
				mRecords.Add(new DatabaseItem()
				{
					Id = i,
					Title = $"Title {i}",
					Description = $"Description of item {i}"
				});
			}
		}
	}
}
