﻿using System;

namespace M2C.Example.Database
{
    public class DatabaseFactory
    {
		/// <summary>
		/// we use a static instance here to avoid creating 1 instance per template
		/// in the playlist. that way they share the same database connection.
		/// </summary>
	    private static Database sInstance;

	    public static IDatabase GetDatabaseInstance()
	    {
		    if (sInstance == null)
				throw new Exception("Database is not yet initialized!");

		    if (!sInstance.IsConnected)
			    throw new Exception("Database is not connected!");

			return sInstance;
	    }

		public static IDatabase CreateDatabaseInstance(string connectionString)
	    {
		    if (sInstance == null)
		    {
			    sInstance = new Database();
			}

			sInstance.Connect(connectionString);

		    return sInstance;
	    }

	    public static void Cleanup()
	    {
		    // cleanup db connection
		    sInstance = null;
	    }
    }
}
