﻿using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template
{
	[Export(typeof(ITemplatePluginDll))]
	[ExportMetadata(kPluginDllGuid, PluginDll.cGUID)]
	[ExportMetadata(kPluginDllName, @"M2C Example 06")]
	[ExportMetadata(kPluginDllDesc, @"Example Icons")]
	[ExportMetadata(kPluginDllGroup, "M2C Examples")]
	public class PluginDll : TemplatePluginDllBase
	{
		public const string cGUID = @"BC30E79B-401E-4386-AC44-781B5B0F1879";
	}
}