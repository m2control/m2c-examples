﻿using System.Windows;
using System.Windows.Controls;

namespace M2C.Example.Template.Icons
{
	/// <summary>
	/// Interaction logic for TemplateControl.xaml
	/// </summary>
	internal partial class IconsControl : UserControl
	{
		public static readonly DependencyProperty ModelProperty = DependencyProperty.Register(
			"Model", typeof(IconsViewModel), typeof(IconsControl), new PropertyMetadata(default(IconsViewModel)));

		public IconsViewModel Model
		{
			get { return (IconsViewModel) GetValue(ModelProperty); }
			set { SetValue(ModelProperty, value); }
		}

		public IconsControl()
		{
			InitializeComponent();
		}

		private void IconsListBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			Model.HandleSelectionChanged(e);

		}

	}
}
