﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using MM.M2Control.Interface;

namespace M2C.Example.Template.Icons
{
	/// <summary>
	/// these exports are used by M2Control to identify the template and its capabilities.
	/// here you define the device that the template can control and to which PluginDll it
	/// belongs. ensure that the TemplateGuid is unique in the system (VS->Tools->Create GUID).
	/// </summary>
	[Export(typeof(ITemplatePlugin))]
	[ExportMetadata(kTemplatePluginDllGuid, PluginDll.cGUID)]
	[ExportMetadata(kTemplateGuid, cGUID)]
	[ExportMetadata(kTemplateName, cTitle)]
	[ExportMetadata(kTemplateDescription, "A simple template to display icons")]
	[ExportMetadata(kTemplateInstanceRule, TemplateInstanceRuleType.Global)]
	[ExportMetadata(kTemplateDeviceHandler, KnownDevices.kNullTemplateDeviceHandlerGuid)]
	[ExportMetadata(kTemplateDefaultAutomationID, "Icons")]
	[ExportMetadata(kTemplateMenuGroup, "M2C Examples")]
	[ExportMetadata(kTemplateIcon, TemplateIconType.ImageMultiple)]
	public class IconsPlugin : TemplatePluginBase
	{
		public const string cGUID = "1FBBE28F-F4B8-4C91-8C16-A770A2C48622";
		public const string cTitle = "Icons";

		// be careful, constructor is called immediately on dll load, before everything else!
		public override ITemplateData InitializeTemplateData(ITemplateDataInitBlock initBlock, List<ITemplateDataValue> data)
		{
			return new IconsData(initBlock, this);
		}
	}
}
