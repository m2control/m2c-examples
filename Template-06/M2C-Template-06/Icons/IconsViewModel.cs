﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using MM.M2Control.Interface;
using MM.M2Control.Wpf;
using MM.M2Control.Wpf.Extensions;
using MM.M2Control.Wpf.Utils;

namespace M2C.Example.Template.Icons
{
	/// <summary>
	/// model is used separate the data and ui from the template data.
	/// this way it's easy to reuse it to support another device.
	/// </summary>
	class IconsViewModel : INotifyPropertyChanged
	{
		private ResourceDictionary mResource;
		private MetroIcon mSelectedIcon;
		private List<MetroIcon> mIcons;
		private List<MetroIcon> mIconsNot;
		private List<MetroIcon> mSelectedIcons = new List<MetroIcon>();
		private List<string> mIconTypesNot = new List<string>();

		private readonly TemplateDataBase mTemplateData;
		private readonly ILogger mLogger;

		/// <summary>
		/// use dependency injection to pass the templateData and host logger to the view model
		/// </summary>
		/// <param name="templateData"></param>
		/// <param name="logger"></param>
		public IconsViewModel(TemplateDataBase templateData, ILogger logger)
		{
			mTemplateData = templateData;
			mLogger = logger;

			// we can bind commands to the control to keep functionality inside
			// the model. that way the control can easily be replaced or changed
			// without changed to controls code behind file
			CmdCopy = new DelegateCommand(CmdCopyExecuteAction, CmdCopyCanExecute);

			mTemplateData.ClearModified();
		}

		#region Properties

		public List<MetroIcon> Icons
		{
			get { return mIcons; }
			set
			{
				mIcons = value;
				OnPropertyChanged();
			}
		}

		public List<MetroIcon> IconsNot
		{
			get { return mIconsNot; }
			set
			{
				mIconsNot = value;
				OnPropertyChanged();
			}
		}

		public List<string> IconTypesNot
		{
			get { return mIconTypesNot; }
			set
			{
				mIconTypesNot = value;
				OnPropertyChanged();
			}
		}

		public MetroIcon SelectedIcon
		{
			get { return mSelectedIcon; }
			set
			{
				mSelectedIcon = value;
				OnPropertyChanged();
			}
		}

		public List<MetroIcon> SelectedIcons
		{
			get { return mSelectedIcons; }
			set
			{
				if (Equals(value, mSelectedIcons)) return;
				mSelectedIcons = value;
				OnPropertyChanged();
			}
		}

		#endregion

		#region Methods


		public void LoadData()
		{
			DispatcherGlue.ThreadSafeCall(LoadIcons);
		}

		public void HandleSelectionChanged(SelectionChangedEventArgs args)
		{
			WpfGlue.HandleSelectionChanged(SelectedIcons, args);

		}
		
		private void LoadIcons()
		{
			mResource = new ResourceDictionary
			{
				Source = new Uri("pack://application:,,,/M2ControlWpf;component/IconResources.xaml", UriKind.RelativeOrAbsolute)
			};

			var uicons = new List<MetroIcon>();
			var uiconsnot = new List<MetroIcon>();  // not found in TemplateIconType

			foreach (DictionaryEntry resource in mResource)
			{
				if (resource.Value == null)
					continue;

				var visual = resource.Value as Canvas;
				if (visual != null)
				{
					var key = (string)resource.Key;

					try
					{
						var iconType = EnumExtension.GetValueFromXmlValueAttribute<TemplateIconType>(key);

						uicons.Add(new MetroIcon(iconType.ToString(), key, visual));
					}
					catch (Exception)
					{
						uiconsnot.Add(new MetroIcon("No_Name", key, visual));
					}
				}
			}

			// Icons found in TemplateIconType
			uicons.Sort((a, b) => string.Compare(a.Name, b.Name, StringComparison.InvariantCulture));
			Icons = new List<MetroIcon>(uicons);

			// Icons not found in TemplateIconType
			uiconsnot.Sort((a, b) => string.Compare(a.Key, b.Key, StringComparison.InvariantCulture));
			IconsNot = new List<MetroIcon>(uiconsnot);

			DumpIconTypeEnum(uicons);

			// no Icons found for TemplateIconType
			var uicontypesnot = new List<string>();
			foreach (var s in Enum.GetNames(typeof(TemplateIconType)))
			{
				if (!Icons.Exists(k => k.Name == s))
					uicontypesnot.Add(s);
			}
			IconTypesNot = new List<string>(uicontypesnot);
		}

		private void DumpIconTypeEnum(List<MetroIcon> icons)
		{
			var sb = new StringBuilder();

			sb.AppendLine("public enum IconType");
			sb.AppendLine("{");

			foreach (var icon in icons)
			{
				//appbar_arrow_collapsed -> arrow_collapsed
				var name = icon.Key.Substring(6);

				// arrow_collapsed -> [arrow, collapsed]
				if (name.Contains("_"))
				{
					var parts = name.Split('_');
					var ucparts = parts.Select(k => CultureInfo.CurrentCulture.TextInfo.ToTitleCase(k));

					name = string.Join("", ucparts);
				}

				sb.AppendLine($"[XmlEnum(\"{icon.Key}\")]");
				sb.AppendLine($"{name},");
				sb.AppendLine();
			}

			sb.AppendLine("}");

			Console.Write(sb.ToString());
		}

		#endregion

		#region Commands

		public ICommand CmdCopy { get; }

		/// <summary>
		/// this is called on ui refresh to define the state of the command
		/// returning false will disable the button
		/// </summary>
		/// <param name="arg"></param>
		/// <returns></returns>
		private bool CmdCopyCanExecute(object arg)
		{
			return SelectedIcons != null && SelectedIcons.Count > 0;
		}

		/// <summary>
		/// the command gets executed. 
		/// </summary>
		/// <param name="obj"></param>
		private void CmdCopyExecuteAction(object obj)
		{
			try
			{
				var names = SelectedIcons
					.OrderBy(k => k.Key)
					.Select(k => k.Key)
					.ToList();

				Clipboard.SetText(string.Join(Environment.NewLine, names));

				mLogger.LogAddDebug($"Copy to clipboard {names.Count} Icons done.");
			}
			catch (Exception e)
			{
				mLogger.LogCatch(nameof(CmdCopyExecuteAction), e);
			}
		}
		#endregion

		public sealed class MetroIcon
		{
			public MetroIcon(string name, string key, Visual visual, bool isHidden = false)
			{
				Name = name;
				Key = key;
				Visual = visual;
				IsHidden = isHidden;
			}

			public string Name { get; private set; }
			public string Key { get; private set; }
			public Visual Visual { get; set; }

			public bool IsHidden { get; set; }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
