﻿using System.Collections.Generic;
using System.Windows.Controls;
using MM.M2Control.Interface;

namespace M2C.Example.Template.Icons
{
	/// <summary>
	/// template data inherits from a template base class to allow usage of the related device
	/// capabilities. override methods of the base class to have full control over sending data
	/// to a device. the base class gets accessible by referencing the M2Control[Vizrt|...] dll.
	/// </summary>
	class IconsData : TemplateDataBase
	{
		private readonly IconsViewModel mModel;

		/// <summary>
		/// this when playlist items get initialized.
		/// the initBlock allows the template to access the interfaces
		/// provided by the client.
		/// </summary>
		/// <param name="initBlock"></param>
		/// <param name="template"></param>
		public IconsData(ITemplateDataInitBlock initBlock, ITemplatePlugin template)
			: base(initBlock, template)
		{
			mModel = new IconsViewModel(this, initBlock.PluginHost.LoggerHandler);
		}

		/// <summary>
		/// return the control for this template - in this case its an wpf control bound
		/// to our view model
		/// </summary>
		/// <returns></returns>
		public override UserControl BuildGui()
		{
			return new IconsControl { Model = mModel };
		}

		public override void LoadData()
		{
			mModel.LoadData();
		}
	}
}
