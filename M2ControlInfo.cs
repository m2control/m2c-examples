namespace MM.M2Control
{
	internal static class M2ControlInfo
	{
		public const string InterfaceLibVersion = "1.51.0.0";
		public const string Version = "1.51.*";
		public const string M2ControlTeam = "M2Control Team";
		public const string M2ControlCopyright = "(C) 2012-19 M2Control Team powered by Verve Consulting GmbH";
	}
}