@echo off

IF EXIST "%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Preview\Common7\Tools\VsDevCmd.bat" goto :f19p
IF EXIST "%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Community\Common7\Tools\VsDevCmd.bat" goto :f19c
IF EXIST "%VS140COMNTOOLS%VsDevCmd.bat" goto :f14
IF EXIST "%VS110COMNTOOLS%VsDevCmd.bat" goto :f11
GOTO :ERR

:f19p
	call "%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Preview\Common7\Tools\VsDevCmd.bat"
	echo USING VS2019 - %ProgramFiles(x86)%\Microsoft Visual Studio\2019\Preview\Common7\Tools
	goto :eof

:f19c
	call "%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Community\Common7\Tools\VsDevCmd.bat"
	echo USING VS2019 %ProgramFiles(x86)%\Microsoft Visual Studio\2019\Community\Common7\Tools
	goto :eof

:f14
	call "%VS140COMNTOOLS%VsDevCmd.bat"
	echo USING VS2015 - %VS140COMNTOOLS%
	goto :eof


:f11
	call "%VS110COMNTOOLS%VsDevCmd.bat"
	echo USING VS2012 - %VS110COMNTOOLS%
	goto :eof

:ERR

echo NO VALID VS ENVIRONMENT FOUND!

pause
exit -1
